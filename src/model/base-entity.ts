export interface BaseEntity {
  // using type any to avoid methods complaining of invalid type
  id?: any;
  syncStatus?: SyncStatus;
}

export enum SyncStatus {
  NONE,
  UPDATE,
  CREATE,
  DELETE
}
