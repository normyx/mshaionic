import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/auth/account.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.page.html',
  styleUrls: ['welcome.page.scss']
})
export class WelcomePage implements OnInit {

  account: Account;

  constructor(private accountService: AccountService, public navController: NavController) {}

  ngOnInit() {
    this.accountService.identity(true).then((account: Account) => {
      this.account = account;
      if (this.account !== null) {
        this.navController.navigateRoot('/menu/home')
      }
    });
  }
}
