import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: '../mha/mha-home/mha-home.module#MhaHomePageModule'
      },
      {
        path: 'workspace',
        loadChildren: '../mha/mha-workspace/mha-workspace.module#MhaWorkspacePageModule'
      },
      {
        path: 'task-project',
        loadChildren: '../mha/mha-task-project/mha-task-project.module#MhaTaskProjectPageModule'
      },
      {
        path: 'wallet',
        loadChildren: '../mha/mha-wallet/mha-wallet.module#MhaWalletPageModule'
      },
      {
        path: 'task',
        loadChildren: '../mha/mha-task/mha-task.module#MhaTaskPageModule'
      },
      {
        path: 'spent',
        loadChildren: '../mha/mha-spent/mha-spent.module#MhaSpentPageModule'
      },
      {
        path: 'spent-config',
        loadChildren: '../mha/mha-spent-config/mha-spent-config.module#MhaSpentConfigPageModule'
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
