import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { filter, map } from 'rxjs/operators';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Spent } from '../../entities/spent';
import { Wallet } from '../../entities/wallet';
import { SyncStatus } from 'src/model/base-entity';
import { Profil } from '../../entities/profil';
import { MhaSpentSharingService } from 'src/app/services/mha/mha-spent-sharing.service/mha-spent-sharing.service';


class WalletProfilSummary {
    profilId: number;
    profilName: string;
    spent: number = 0;
    amount: number = 0;

    getBalance() {
        return this.spent - this.amount;
    }
}



@Component({
    selector: 'mha-page-wallet-detail-summary',
    templateUrl: 'mha-wallet-detail-summary.html',
    styleUrls: ['./mha-wallet.scss'],
})
export class MhaWalletDetailSummaryPage implements OnInit {

    wallet: Wallet;
    spents: Spent[];
    profilSummaries: WalletProfilSummary[] = [];

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private spentService: MhaSpentService,
        private spentSharingService: MhaSpentSharingService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController
    ) { }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh();
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.activatedRoute.data.subscribe((response) => {
            this.wallet = response.data;
            this.spentService.findWhereParent([this.wallet]).pipe(
                filter((res: HttpResponse<Spent[]>) => res.ok),
                map((res: HttpResponse<Spent[]>) => res.body))
                .subscribe(
                    async (spents: Spent[]) => {
                        this.spents = spents.filter(s => s.syncStatus != SyncStatus.DELETE);
                        for (let spent of this.spents) {
                            let profilSummary = this.profilSummaries.find(ps => ps.profilId === spent.spenderId);
                            if (!profilSummary) {
                                profilSummary = new WalletProfilSummary();
                                profilSummary.profilId = spent.spenderId;
                                profilSummary.profilName = spent.spenderDisplayName;
                                this.profilSummaries.push(profilSummary);
                            }

                            profilSummary.spent += spent.amount;
                            let spentSharings = await this.spentSharingService.findWhereParent([spent]).toPromise();
                            if (spentSharings.body) {
                                for (let spentSharing of spentSharings.body) {
                                    let profilSummary2 = this.profilSummaries.find(ps => ps.profilId === spentSharing.sharingProfilId);
                                    if (!profilSummary2) {
                                        profilSummary2 = new WalletProfilSummary();
                                        profilSummary2.profilId = spentSharing.sharingProfilId;
                                        profilSummary2.profilName = spentSharing.sharingProfilDisplayName;
                                        this.profilSummaries.push(profilSummary2);

                                    }
                                    profilSummary2.amount += spentSharing.amountShare;
                                }
                            }
                        }

                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    getTotalSpent() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.spent);
        return total;
    }

    getTotalAmount() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.amount);
        return total;
    }
    getTotalBalance() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.getBalance());
        return total;
    }

    goUpdatePage($event) {
        this.navController.navigateForward('/menu/wallet/' + this.wallet.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    goBack() {
        this.navController.navigateRoot('/menu/wallet');
    }

    segmentChanged($event) {
        if (this.wallet) {
            this.navController.navigateRoot('/menu/wallet/' + this.wallet.id + '/view');
        }
    }
}
