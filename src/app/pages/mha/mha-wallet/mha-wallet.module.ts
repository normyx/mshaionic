import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MhaSpentComponentModule } from 'src/app/components/mha/mha-spent/mha-spent.component.module';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaProfilDataComponentModule } from '../../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../../components/mha/mha-profil/mha-profile.component.module';
import { Wallet } from '../../entities/wallet';
import { MhaWalletPage } from './mha-wallet';
import { MhaWalletDetailPage } from './mha-wallet-detail';
import { MhaWalletUpdatePage } from './mha-wallet-update';
import { MhaWalletDetailSummaryPage } from './mha-wallet-detail-summary';



@Injectable({ providedIn: 'root' })
export class MhaWalletResolve implements Resolve<Wallet> {
  constructor(private service: MhaWalletService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Wallet>) => response.ok),
        map((wallet: HttpResponse<Wallet>) => wallet.body)
      );
    }
    return of(new Wallet());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaWalletFromWorkspaceResolve implements Resolve<Wallet> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let wallet = new Wallet();
      wallet.workspaceId = workspaceId;
      return of(wallet);

    }
    return of(new Wallet());
  }
}

const routes: Routes = [
  {
    path: '',
    component: MhaWalletPage
  },
  
  {
    path: ':id/view',
    component: MhaWalletDetailPage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
    
  },
  {
    path: ':id/summary',
    component: MhaWalletDetailSummaryPage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
    
  },
  {
    path: ':id/edit',
    component: MhaWalletUpdatePage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':workspaceId/new',
    component: MhaWalletUpdatePage,
    resolve: {
      data: MhaWalletFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaSpentComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaWalletPage, MhaWalletDetailPage, MhaWalletDetailSummaryPage,MhaWalletUpdatePage],
  //entryComponents: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MhaWalletPageModule {}
