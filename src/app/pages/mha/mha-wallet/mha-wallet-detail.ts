import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, PopoverController, ToastController, Events, IonInfiniteScroll } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Spent } from '../../entities/spent';
import { Wallet } from '../../entities/wallet';
import { SyncStatus } from 'src/model/base-entity';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';

@Component({
    selector: 'mha-page-wallet-detail',
    templateUrl: 'mha-wallet-detail.html',
    styleUrls: ['./mha-wallet.scss'],
})
export class MhaWalletDetailPage implements OnInit {

    private static SCROLL_PAGE_LENGTH = 25;

    @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

    wallet: Wallet;
    spents: Spent[];
    scrolledSpents: Spent[] = [];
    hasModification: boolean;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private spentService: MhaSpentService,
        private walletService: MhaWalletService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController,
        public events: Events,
        private zone: NgZone
    ) {
        this.events.subscribe('updateWallet', () => {
            this.zone.run(() => {
                this.loadAll();
            });
        });
    }

    ngOnInit() {
    }


    ionViewWillEnter() {
        this.loadAll();
    }

    loadScrolledData(event) {
        setTimeout(() => {
            this.fillScrolledSpents();
            event.target.complete();
        }, 500);
    }

    fillScrolledSpents(loading?: boolean) {
        if (loading) {
            this.scrolledSpents = [];
        }
        if (this.spents && (loading || !this.infiniteScroll.disabled)) {
            const scrolledLength = this.scrolledSpents.length;
            for (let i = 0; i < MhaWalletDetailPage.SCROLL_PAGE_LENGTH && i < this.spents.length - scrolledLength; i++) {
                this.scrolledSpents.push(this.spents[this.scrolledSpents.length]);
            }
        }
        if (this.spents.length == this.scrolledSpents.length) {
            this.infiniteScroll.disabled = true;
        }
    }


    async loadAll(refresher?) {
        this.hasModification = this.walletService.hasModification() || this.spentService.hasModification();
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh();
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.walletService.hasModification() || this.spentService.hasModification();
        this.activatedRoute.data.subscribe((response) => {
            this.wallet = response.data;
            this.spentService.findWhereParent([this.wallet]).pipe(
                filter((res: HttpResponse<Spent[]>) => res.ok),
                map((res: HttpResponse<Spent[]>) => res.body))
                .subscribe(
                    (response: Spent[]) => {

                        this.spents = response.filter(s => { return s.syncStatus != SyncStatus.DELETE });
                        this.fillScrolledSpents(true);
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    newSpent() {
        this.navController.navigateForward('/menu/spent/' + this.wallet.id + '/new');
    }

    goBack() {
        this.navController.navigateRoot('/menu/wallet');
    }

    trackId(index: number, item: Spent) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/wallet/' + this.wallet.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    segmentChanged($event) {
        if (this.wallet) {
            this.navController.navigateRoot('/menu/wallet/' + this.wallet.id + '/summary');
        }
    }

}
