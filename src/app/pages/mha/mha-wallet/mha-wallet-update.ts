import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, PopoverController, ToastController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MhaProfileChooserPopoverComponent } from 'src/app/components/mha/mha-profil/mha-profile-chooser-popover.component/mha-profile-chooser-popover.component';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { Profil } from '../../entities/profil';
import { Wallet } from '../../entities/wallet';
import { Workspace } from '../../entities/workspace';

@Component({
    selector: 'page-mha-wallet-update',
    templateUrl: 'mha-wallet-update.html'
})
export class MhaWalletUpdatePage implements OnInit {

    wallet: Wallet;
    workspace: Workspace;
    profils: Profil[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        workspaceId: [null, []],
        owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController,
        protected formBuilder: FormBuilder,
        protected platform: Platform,
        protected toastCtrl: ToastController,
        private workspaceService: MhaWorkspaceService,
        private walletService: MhaWalletService,
        private popoverCtrl: PopoverController
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {

        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.wallet = response.data;
            this.isNew = this.wallet.id === null || this.wallet.id === undefined;
        });
    }

    updateForm(wallet: Wallet) {
        this.form.patchValue({
            id: wallet.id,
            label: wallet.label,
            workspaceId: wallet.workspaceId,
            owners: wallet.owners,
        });
        this.workspaceService.find(wallet.workspaceId).subscribe(data => {
            this.workspace = data.body;
        }, (error) => this.onError(error));
    }

    save() {
        this.isSaving = true;
        const wallet = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.walletService.update(wallet));
        } else {
            this.subscribeToSaveResponse(this.walletService.create(wallet));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Wallet>>) {
        result.subscribe((res: HttpResponse<Wallet>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
            this.wallet = response.body;
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `Wallet ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();
        this.navController.navigateForward('/menu/wallet/' + this.wallet.id + '/view');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): Wallet {
        return {
            ...new Wallet(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
            syncStatus: this.wallet.syncStatus
        };
    }

    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    async chooseProfile(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: MhaProfileChooserPopoverComponent,
            event: ev,
            animated: true,
            showBackdrop: true,
            componentProps: { "profils": this.wallet.owners, "availableProfils": this.getAvailableProfils(), "canBeWithNoActor": false }
        });
        popover.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.wallet.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.wallet.owners });
            }
        });
        return await popover.present();
    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.wallet.owners) {
            this.wallet.owners = new Array<Profil>();
        }
        for (let workspaceProfil of this.workspace.owners) {
            if (!this.wallet.owners.some(e => e.id === workspaceProfil.id)) {
                profils.push(workspaceProfil);
            }
        }
        return profils;

    }

    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.wallet.label + ' ? Toutes les dépenses correspondantes seront également supprimées.',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.walletService.delete(this.wallet.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/wallet');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }

    viewSpentConfigs() {
        this.navController.navigateRoot('/menu/spent-config/' + this.wallet.id);
    }
}
