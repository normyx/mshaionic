import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { Wallet } from '../../entities/wallet';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { SyncStatus } from 'src/model/base-entity';


@Component({
  selector: 'app-mha-wallet',
  templateUrl: './mha-wallet.html',
  styleUrls: ['./mha-wallet.scss'],
})
export class MhaWalletPage {

  wallets: Wallet[];

  constructor(
    private walletService: MhaWalletService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform) { }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh();
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.walletService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<Wallet[]>) => res.ok),
      map((res: HttpResponse<Wallet[]>) => res.body)
    )
      .subscribe(
        (response: Wallet[]) => {
          this.wallets = response.filter(w => w.syncStatus != SyncStatus.DELETE);
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: Wallet) {
    return item.id;
  }
  view(wallet: Wallet) {
    this.navController.navigateForward('/menu/wallet/' + wallet.id + '/view');
  }

  newWallet() {
    this.navController.navigateForward('/menu/wallet/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }

  


}
