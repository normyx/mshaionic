import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { getPriorityColor } from '../../../shared/util/helpers';
import { Profil } from '../../entities/profil';
import { Task } from '../../entities/task';
import { TaskProject } from '../../entities/task-project';
import { MhaProfileChooserPopoverComponent } from '../../../components/mha/mha-profil/mha-profile-chooser-popover.component/mha-profile-chooser-popover.component';
import { MhaTaskPriorityPriorityChooserPopoverComponent } from './mha-task-priority-chooser-popover/mha-task-priority-chooser-popover.component';

@Component({
    selector: 'mha-page-task-update',
    templateUrl: 'mha-task-update.html',
    styleUrls: ['./mha-task.scss']
})
export class MhaTaskUpdatePage implements OnInit {

    task: Task;
    taskProject: TaskProject;
    dueDateDp: any;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    radio_priorities = [
        {
            name: 'Urgente',
            value: '1',
            color: 'danger'
        },
        {
            name: 'Moyenne',
            value: '2',
            color: 'primary'
        },
        {
            name: 'Faible',
            value: '3',
            color: 'secondary'
        },
        {
            name: 'Aucune',
            value: '4',
            color: 'light'
        },

    ];

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        description: [null, []],
        done: ['false', [Validators.required]],
        dueDate: [null, []],
        priority: [null, [Validators.required]],
        taskProjectId: [null, []],
        owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private alertController: AlertController, 
        private mhaTaskService: MhaTaskService,
        private taskProjectService: MhaTaskProjectService,
        private popoverCtrl: PopoverController
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {

        //this.profilService.query()
        //    .subscribe(data => { this.profils = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {

            this.task = response.data;
            this.isNew = this.task.id === null || this.task.id === undefined;
            if (this.isNew) {
                this.task.priority = 4;
            }
            this.updateForm(response.data);
        });
    }

    updateForm(task: Task) {
        this.form.patchValue({
            id: task.id,
            label: task.label,
            description: task.description,
            done: task.done,
            dueDate: task.dueDate,
            priority: task.priority,
            taskProjectId: task.taskProjectId,
            owners: task.owners,
        });

        this.taskProjectService.find(task.taskProjectId).subscribe(data => {
            this.taskProject = data.body;
        }, (error) => this.onError(error));


    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.task.owners) {
            this.task.owners = new Array<Profil>();
        }
        for (let taskProjectProfil of this.taskProject.owners) {
            if (!this.task.owners.some(e => e.id === taskProjectProfil.id)) {
                profils.push(taskProjectProfil);
            }
        }
        return profils;

    }



    save() {
        this.isSaving = true;
        const task = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.mhaTaskService.update(task));
        } else {
            this.subscribeToSaveResponse(this.mhaTaskService.create(task));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Task>>) {
        result.subscribe((res: HttpResponse<Task>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `Task ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();

        this.navController.navigateBack('/menu/task-project/' + this.task.taskProjectId + '/view');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): Task {
        return {
            ...new Task(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            description: this.form.get(['description']).value,
            done: this.form.get(['done']).value,
            dueDate: this.form.get(['dueDate']).value,
            priority: this.form.get(['priority']).value,
            taskProjectId: this.form.get(['taskProjectId']).value,
            owners: this.form.get(['owners']).value,
        };
    }

    compareTaskProject(first: TaskProject, second: TaskProject): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackTaskProjectById(index: number, item: TaskProject) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    priorityRadioSelect(event) {
        this.form.patchValue({ 'priority': event.detail.value });
    }

    async choosePriority(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: MhaTaskPriorityPriorityChooserPopoverComponent,
            event: ev,
            animated: true,
            showBackdrop: true,
            translucent: true,
            componentProps: { "priority": this.task.priority }
        });
        popover.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.form.patchValue({ 'priority': dataReturned.data });
                this.task.priority = parseInt(dataReturned.data);
            }
        });
        return await popover.present();
    }

    async chooseProfile(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: MhaProfileChooserPopoverComponent,
            event: ev,
            animated: true,
            showBackdrop: true,
            translucent: true,
            cssClass: "profil-popover",
            componentProps: { "profils": this.task.owners, "availableProfils": this.getAvailableProfils() }
        });
        popover.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.task.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.task.owners });
            }
        });
        return await popover.present();
    }


    getPriorityColor(): string {
        return getPriorityColor(this.task.priority);
    }

 
    async deleteModal() {
        const alert = await this.alertController.create({
          header: 'Confirmer vous la suppression de ' + this.task.label + '?',
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary'
            }, {
              text: 'Supprimer',
              handler: () => {
                this.mhaTaskService.delete(this.task.id).subscribe(() => {
                  this.navController.navigateRoot('/menu/task-project/' + this.task.taskProjectId + '/view');
                  
                });
    
              }
            }
          ]
        });
        await alert.present();
      }
}
