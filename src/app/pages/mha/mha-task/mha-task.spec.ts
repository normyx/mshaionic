import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhaTaskPage } from './mha-task';

describe('MhaTasksPage', () => {
  let component: MhaTaskPage;
  let fixture: ComponentFixture<MhaTaskPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhaTaskPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhaTaskPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
