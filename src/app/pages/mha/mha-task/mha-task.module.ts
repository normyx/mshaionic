import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { Task } from '../../entities/task';
import { MhaProfilDataComponentModule } from '../../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../../components/mha/mha-profil/mha-profile.component.module';
import { MhaTaskPriorityPriorityChooserPopoverComponent } from './mha-task-priority-chooser-popover/mha-task-priority-chooser-popover.component';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { MhaTaskUpdatePage } from './mha-task-update';


@Injectable({ providedIn: 'root' })
export class MhaTaskResolve implements Resolve<Task> {
  constructor(private service:MhaTaskService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Task>) => response.ok),
        map((task: HttpResponse<Task>) => task.body)
      );
    }
    return of(new Task());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTaskFromTaskProjectResolve implements Resolve<Task> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> {
    const taskProjectId = route.params.taskProjectId ? route.params.taskProjectId : null;
    if (taskProjectId) {
      let task = new Task();
      task.taskProjectId = taskProjectId;
      return of(task);

    }
    return of(new Task());
  }
}

const routes: Routes = [
  {
    path: ':id/edit',
    component: MhaTaskUpdatePage,
    resolve: {
      data: MhaTaskResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':taskProjectId/new',
    component: MhaTaskUpdatePage,
    resolve: {
      data: MhaTaskFromTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaTaskUpdatePage, MhaTaskPriorityPriorityChooserPopoverComponent],
  entryComponents: [MhaTaskPriorityPriorityChooserPopoverComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaTaskPageModule { }
