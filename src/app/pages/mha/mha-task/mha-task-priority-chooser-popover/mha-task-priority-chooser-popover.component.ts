import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-mha-task-priority-chooser-popover',
  templateUrl: './mha-task-priority-chooser-popover.component.html',
  styleUrls: ['./mha-task-priority-chooser-popover.component.scss'],
})
export class MhaTaskPriorityPriorityChooserPopoverComponent implements OnInit {

  priority: number;

  radio_priorities = [
    {
      name: 'Urgente',
      value: '1',
      color: 'danger'
    },
    {
      name: 'Moyenne',
      value: '2',
      color: 'primary'
    },
    {
      name: 'Faible',
      value: '3',
      color: 'secondary'
    },
    {
      name: 'Aucune',
      value: '4',
      color: 'light'
    },

  ];
  constructor(private popController: PopoverController) { }

  ngOnInit() { }

  async priorityRadioSelect(event) {
    await this.popController.dismiss(event.detail.value);
  }
}
