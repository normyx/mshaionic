import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhaTaskPriorityPriorityChooserPopoverComponent } from './mha-task-priority-chooser-popover.component';

describe('ChoosePriorityComponent', () => {
  let component: MhaTaskPriorityPriorityChooserPopoverComponent;
  let fixture: ComponentFixture<MhaTaskPriorityPriorityChooserPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhaTaskPriorityPriorityChooserPopoverComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhaTaskPriorityPriorityChooserPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
