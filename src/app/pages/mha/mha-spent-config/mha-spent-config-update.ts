import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MhaSpentConfigService } from 'src/app/services/mha/mha-spent.-config.service/mha-spent-config.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { Profil } from '../../entities/profil';
import { SpentConfig } from '../../entities/spent-config';
import { Wallet } from '../../entities/wallet';
import { MhaSpentSharingConfigService } from 'src/app/services/mha/mha-spent-sharing-config.service/mha-spent-sharing-config.service';
import { SpentSharingConfig } from '../../entities/spent-sharing-config';

@Component({
    selector: 'page-mha-spent-config-update',
    templateUrl: 'mha-spent-config-update.html',
    styleUrls: ['mha-spent-config.scss']
})
export class MhaSpentConfigUpdatePage implements OnInit {

    spentConfig: SpentConfig;
    wallet: Wallet;
    profils: Profil[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController,
        protected formBuilder: FormBuilder,
        protected platform: Platform,
        protected toastCtrl: ToastController,
        private walletService: MhaWalletService,
        private spentSharingConfigService: MhaSpentSharingConfigService,
        private spentConfigService: MhaSpentConfigService,
    ) {

        

    }

    addSpentSharingConfig() {
        return this.formBuilder.group({
            id: '',
            profilId: '',
            profilDisplayName: '',
            share: '',
            spentConfigId: '',
            syncStatus: ''
        })
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            id: [],
            label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
            walletId: [null, []],
            spentSharingConfigs: this.formBuilder.array([]),
            syncStatus: ''
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });
        this.activatedRoute.data.subscribe((response) => {
            this.spentConfig = response.data;
            if (!this.spentConfig.spentSharingConfigs) { this.spentConfig.spentSharingConfigs = []; }
            this.isNew = this.spentConfig.id === null || this.spentConfig.id === undefined;
            this.walletService.find(this.spentConfig.walletId).subscribe(data => {
                this.wallet = data.body;
                this.spentSharingConfigService.findWhereParent([this.spentConfig]).subscribe(data2 => {
                    this.spentConfig.spentSharingConfigs = data2.body;
                    this.createSpentSharingConfigIfNeeded();
                    const spentSharingConfigsArray = this.form.get('spentSharingConfigs') as FormArray;
                    for (let i = 0; i < this.spentConfig.spentSharingConfigs.length; i++) {
                        spentSharingConfigsArray.push(this.addSpentSharingConfig());
                    }
                    this.updateForm(this.spentConfig);
                }, (error) => this.onError(error));
            }, (error) => this.onError(error));
        });
        
    }

    private createSpentSharingConfigIfNeeded() {
        for (let profil of this.wallet.owners) {
            let spentSharingConfigExists = this.spentConfig.spentSharingConfigs.find(e => { return e.profilId == profil.id });
            if (!spentSharingConfigExists || spentSharingConfigExists == null) {
                spentSharingConfigExists = new SpentSharingConfig();
                spentSharingConfigExists.share = 1;
                
                spentSharingConfigExists.profilId = profil.id;
                spentSharingConfigExists.profilDisplayName = profil.displayName;
                spentSharingConfigExists.spentConfigId = this.spentConfig.id;
                this.spentConfig.spentSharingConfigs.push(spentSharingConfigExists);
            }
        }

    }

    updateForm(spentConfig: SpentConfig) {
        this.form.patchValue({
            id: spentConfig.id,
            label: spentConfig.label,
            walletId: spentConfig.walletId,
            spentSharingConfigs: spentConfig.spentSharingConfigs,
            syncStatus: spentConfig.syncStatus
        });
        /*this.walletService.find(spentConfig.walletId).subscribe(data => {
            this.wallet = data.body;
        }, (error) => this.onError(error));*/
    }

    save() {
        this.isSaving = true;
        const spentConfig = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.spentConfigService.update(spentConfig));
        } else {
            this.subscribeToSaveResponse(this.spentConfigService.create(spentConfig));
        }
        for (let spentSharingConfig of spentConfig.spentSharingConfigs) {
            if (!spentSharingConfig.id) {
                this.spentSharingConfigService.create(spentSharingConfig);
            } else {
                this.spentSharingConfigService.update(spentSharingConfig);
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<SpentConfig>>) {
        result.subscribe((res: HttpResponse<SpentConfig>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
            this.spentConfig = response.body;
            for (let spentSharingConfig of this.spentConfig.spentSharingConfigs) {
                spentSharingConfig.spentConfigId = this.spentConfig.id;
            }
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `Wallet ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();
        this.navController.navigateForward('/menu/spent-config/' + this.wallet.id);
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): SpentConfig {
        return {
            ...new SpentConfig(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            walletId: this.form.get(['walletId']).value,
            syncStatus: this.form.get(['syncStatus']).value,
            spentSharingConfigs: this.form.get(['spentSharingConfigs']).value,
        };
    }

    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.spentConfig.label + ' ? ',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.spentConfigService.delete(this.spentConfig.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/spent-config/'+this.wallet.id);

                        });

                    }
                }
            ]
        });
        await alert.present();
    }

    goBack() {
        this.navController.navigateRoot('/menu/spent-config/'+this.wallet.id);
    }
    
}
