import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MhaSpentConfigService } from 'src/app/services/mha/mha-spent.-config.service/mha-spent-config.service';
import { MhaProfilDataComponentModule } from '../../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../../components/mha/mha-profil/mha-profile.component.module';
import { SpentConfig } from '../../entities/spent-config';
import { MhaSpentConfigPage } from './mha-spent-config';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { Wallet } from '../../entities/wallet';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaSpentConfigUpdatePage } from './mha-spent-config-update';



@Injectable({ providedIn: 'root' })
export class MhaSpentConfigResolve implements Resolve<SpentConfig> {
  constructor(private service: MhaSpentConfigService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpentConfig> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentConfig>) => response.ok),
        map((spentConfig: HttpResponse<SpentConfig>) => spentConfig.body)
      );
    }
    return of(new SpentConfig());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigListFromWalletResolve implements Resolve<Wallet> {
  constructor(private spentConfigService: MhaSpentConfigService, private walletService: MhaWalletService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    if (walletId) {
      return this.walletService.find(walletId).pipe(
        filter((response: HttpResponse<Wallet>) => response.ok),
        map((wallet: HttpResponse<Wallet>) => wallet.body)
      );
    }
    return of(new Wallet());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigFromWalletResolve implements Resolve<SpentConfig> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    if (walletId) {
      let spentConfig = new SpentConfig();
      spentConfig.walletId = walletId;
      return of(spentConfig);

    }
    return of(new SpentConfig());
  }
}


const routes: Routes = [
  {
    path: ':walletId',
    component: MhaSpentConfigPage,
    resolve: {
      data: MhaSpentConfigListFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
  },

  /*{
    path: ':id/view',
    component: MhaWalletDetailPage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
    
  },
  {
    path: ':id/summary',
    component: MhaWalletDetailSummaryPage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
    
  },*/
  {
    path: ':id/edit',
    component: MhaSpentConfigUpdatePage,
    resolve: {
      data: MhaSpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':walletId/new',
    component: MhaSpentConfigUpdatePage,
    resolve: {
      data: MhaSpentConfigFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaSpentConfigPage, MhaSpentConfigUpdatePage],
  //entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaSpentConfigPageModule { }
