import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaSpentConfigService } from 'src/app/services/mha/mha-spent.-config.service/mha-spent-config.service';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { SpentConfig } from '../../entities/spent-config';
import { Wallet } from '../../entities/wallet';


@Component({
  selector: 'app-mha-spent-config',
  templateUrl: './mha-spent-config.html',
  styleUrls: ['./mha-spent-config.scss'],
})
export class MhaSpentConfigPage {

  wallet: Wallet;
  spentConfigs: SpentConfig[];

  constructor(
    private spentConfigService: MhaSpentConfigService,
    private activatedRoute: ActivatedRoute,
    private asyncService: MhaAsyncService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform) { }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh();
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activatedRoute.data.subscribe((response) => {
      this.wallet = response.data;
      this.spentConfigService.findWhereParent([this.wallet]).pipe(
        filter((res: HttpResponse<SpentConfig[]>) => res.ok),
        map((res: HttpResponse<SpentConfig[]>) => res.body))
        .subscribe(
          (response: SpentConfig[]) => {
            this.spentConfigs = response.filter(s => { return s.syncStatus != SyncStatus.DELETE }).sort((a: SpentConfig, b: SpentConfig) => {
              return b.label.localeCompare(a.label);
            });
          },
          async (error) => {
            console.error(error);
            const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
            toast.present();
          });
    });
  }

  trackId(index: number, item: SpentConfig) {
    return item.id;
  }

  edit(spentConfig: SpentConfig) {
    this.navController.navigateForward('/menu/spent-config/' + spentConfig.id + '/edit');
  }
  new() {
    this.navController.navigateForward('/menu/spent-config/' + this.wallet.id + '/new');
  }

  goBack() {
    this.navController.navigateRoot('/menu/wallet/' + this.wallet.id + '/edit');
  }


}
