import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { filter, map } from 'rxjs/operators';
import { Workspace } from '../../entities/workspace';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaWorkspaceService } from '../../../services/mha/mha-workspace.service/mha-workspace.service';

@Component({
    selector: 'mha-page-workspace',
    templateUrl: 'mha-workspace.html'
})
export class MhaWorkspacePage {
    workspaces: Workspace[];
    selectedWorkspace: Workspace;
    // todo: add pagination

    constructor(
        private navController: NavController,
        private workspaceService: MhaWorkspaceService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
    ) {
        this.workspaces = [];
        this.selectedWorkspace = this.workspaceService.getStoredActiveWorkspace();
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof(refresher) !== 'undefined') {
            await this.asyncService.refresh();
            setTimeout(() => {
                refresher.target.complete();
            }, 2000);
        }
        this.workspaceService.findWhereParent(null).pipe(
            filter((res: HttpResponse<Workspace[]>) => res.ok),
            map((res: HttpResponse<Workspace[]>) => res.body)
        )
        .subscribe(
            (response: Workspace[]) => {
                this.workspaces = response;
            },
            async (error) => {
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
            
    }

    trackId(index: number, item: Workspace) {
        return item.id;
    }


    selectWorkspace(workspace: Workspace) {
        this.workspaceService.setStoredActiveWorkspace(workspace);
        this.navController.navigateForward('/menu/home');
    }
}
