import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { Workspace, WorkspaceService } from '../../entities/workspace';
import { MhaWorkspacePage } from './mha-workspace';

@Injectable({ providedIn: 'root' })
export class WorkspaceResolve implements Resolve<Workspace> {
  constructor(private service: WorkspaceService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Workspace> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Workspace>) => response.ok),
        map((workspace: HttpResponse<Workspace>) => workspace.body)
      );
    }
    return of(new Workspace());
  }
}

const routes: Routes = [
  {
    path: '',
    component: MhaWorkspacePage,
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
];


@NgModule({
  declarations: [
    MhaWorkspacePage,
  ],
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ]
})
export class MhaWorkspacePageModule {
}
