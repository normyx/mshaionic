import { Component, Injectable, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { debounceTime } from 'rxjs/operators';
import { NetworkService } from '../../../services/mha/mha-network.service';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Workspace } from '../../entities/workspace';
import { MhaWorkspaceService } from '../../../services/mha/mha-workspace.service/mha-workspace.service';

@Injectable()
@Component({
  selector: 'app-mha-home',
  templateUrl: './mha-home.page.html',
  styleUrls: ['./mha-home.page.scss'],
})
export class MhaHomePage implements OnInit {

  isConnected: boolean;
  workspace:Workspace;

  constructor(
    protected asyncService: MhaAsyncService,
    protected workspaceService: MhaWorkspaceService,
    public plt: Platform,
    public networkService: NetworkService

  ) {

  }

  ngOnInit() {
    this.networkSubscriber();
    this.workspace = this.workspaceService.getStoredActiveWorkspace();
  }

  networkSubscriber(): void {
    this.networkService
      .getNetworkStatus()
      .pipe(debounceTime(300))
      .subscribe((connected: boolean) => {
        this.isConnected = connected;
        //this.handleNotConnected(connected);
      });
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh();
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
  }
}
