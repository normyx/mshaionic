import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NetworkService } from 'src/app/services/mha/mha-network.service';
import { MhaHomePage } from './mha-home.page';



const routes: Routes = [
  {
    path: '',
    component: MhaHomePage
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaHomePage],
  providers: [NetworkService]
})
export class MhaHomePageModule { }
