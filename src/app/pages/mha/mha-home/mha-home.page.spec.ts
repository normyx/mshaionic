import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhaHomePage } from './mha-home.page';

describe('MhaHomePage', () => {
  let component: MhaHomePage;
  let fixture: ComponentFixture<MhaHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhaHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhaHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
