import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, ToastController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { Profil } from '../../entities/profil';
import { Spent } from '../../entities/spent';
import { Wallet } from '../../entities/wallet';
import { SpentSharing } from '../../entities/spent-sharing';
import { MhaSpentSharingService } from 'src/app/services/mha/mha-spent-sharing.service/mha-spent-sharing.service';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import * as moment from 'moment';
import { MONTH_SHORT_NAME } from 'src/app/shared/constants/input.constants';
import { MhaSpentConfigService } from 'src/app/services/mha/mha-spent.-config.service/mha-spent-config.service';
import { SpentConfig } from '../../entities/spent-config';
import { MhaSpentSharingConfigService } from 'src/app/services/mha/mha-spent-sharing-config.service/mha-spent-sharing-config.service';


@Component({
    selector: 'page-mha-spent-update',
    templateUrl: 'mha-spent-update.html',
    styleUrls: ['mha-spent.scss']
})
export class MhaSpentUpdatePage implements OnInit {
    readonly customMonthShortName = MONTH_SHORT_NAME;

    spent: Spent;
    wallet: Wallet;
    spentConfigs: SpentConfig[];
    spentDateDp: any;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;
    totalShare: number;

    form: FormGroup;

    addSpentSharing() {
        return this.formBuilder.group({
            id: '',
            amountShare: '',
            sharingProfilId: '',
            sharingProfilDisplayName: '',
            share: '',
            spentId: '',
            syncStatus: ''
        })
    }

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private alertController: AlertController,
        private walletService: MhaWalletService,
        private spentService: MhaSpentService,
        private spentSharingService: MhaSpentSharingService,
        private spentConfigService: MhaSpentConfigService,
        private spentSharingConfigService: MhaSpentSharingConfigService,
        private profilService: MhaActiveProfilService
    ) {

        

    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            id: [],
            label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
            description: [null, [Validators.maxLength(200)]],
            amount: [null, [Validators.required]],
            spentDate: [null, [Validators.required]],
            //lastUpdateDate: [null, [Validators.required]],
            confirmed: ['false', [Validators.required]],
            spenderId: [null, []],
            walletId: [null, []],
            spentSharings: this.formBuilder.array([]),
            syncStatus: ''
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

        this.activatedRoute.data.subscribe((response) => {
            this.spent = response.data;
            if (!this.spent.spentSharings) { this.spent.spentSharings = []; }
            this.isNew = this.spent.id === null || this.spent.id === undefined;
            this.walletService.find(this.spent.walletId).subscribe(data => {
                this.wallet = data.body;
                this.spentConfigService.findWhereParent([this.wallet]).subscribe(spentConfigResponse => {this.spentConfigs = spentConfigResponse.body;})
                this.spentSharingService.findWhereParent([this.spent]).subscribe(data2 => {
                    this.spent.spentSharings = data2.body;
                    this.createSpentSharingIfNeeded();
                    this.computeTotalShare();
                    this.amountChange(null);
                    const spentSharingsArray = this.form.get('spentSharings') as FormArray;
                    for (let i = 0; i < this.spent.spentSharings.length; i++) {
                        spentSharingsArray.push(this.addSpentSharing());
                    }
                    this.updateForm(this.spent);
                }, (error) => this.onError(error));
            }, (error) => this.onError(error));
        });
    }

    private createSpentSharingIfNeeded() {
        let sharesum = 0;
        this.spent.spentSharings.forEach(ss => {sharesum += ss.share});
        for (let profil of this.wallet.owners) {
            let spentSharingExists = this.spent.spentSharings.find(e => { return e.sharingProfilId == profil.id });
            if (!spentSharingExists || spentSharingExists == null) {
                spentSharingExists = new SpentSharing();
                spentSharingExists.share = sharesum == 0 ? 1 : 0;
                spentSharingExists.amountShare = 0;
                spentSharingExists.sharingProfilId = profil.id;
                spentSharingExists.sharingProfilDisplayName = profil.displayName;
                spentSharingExists.spentId = this.spent.id;
                this.spent.spentSharings.push(spentSharingExists);
            }
        }

    }

    updateForm(spent: Spent) {
        this.form.patchValue({
            id: spent.id,
            label: spent.label,
            description: spent.description,
            amount: spent.amount,
            spentDate: spent.spentDate ? spent.spentDate : moment().format("YYYY/MM/DD"),
            //lastUpdateDate: spent.lastUpdateDate,
            confirmed: spent.confirmed,
            spenderId: spent.spenderId ? spent.spenderId : this.profilService.getStoredProfil(),
            walletId: spent.walletId,
            spentSharings: spent.spentSharings,
            syncStatus: spent.syncStatus
        });


    }

    save() {
        this.isSaving = true;
        const spent = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.spentService.update(spent));
        } else {
            this.subscribeToSaveResponse(this.spentService.create(spent));
        }
        for (let spentSharing of spent.spentSharings) {
            if (!spentSharing.id) {
                this.spentSharingService.create(spentSharing);
            } else {
                this.spentSharingService.update(spentSharing);
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Spent>>) {
        result.subscribe((res: HttpResponse<Spent>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'mise à jour';
        if (response.status === 201) {
            action = 'créée';
            this.spent = response.body;
            for (let spentSharing of this.spent.spentSharings) {
                spentSharing.spentId = this.spent.id;
            }
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `Dépense ${action} avec succès.`, duration: 1000, position: 'bottom' });
        toast.present();
        this.navController.navigateBack('/menu/wallet/' + this.spent.walletId + '/view');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Erreur lors de la sauvegarde', duration: 2000, position: 'bottom' });
        toast.present();
    }

    private createFromForm(): Spent {
        return {
            ...new Spent(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            description: this.form.get(['description']).value,
            amount: Number(this.form.get(['amount']).value),
            spentDate: this.form.get(['spentDate']).value,
            lastUpdateDate: moment(),
            confirmed: this.form.get(['confirmed']).value,
            spenderId: this.form.get(['spenderId']).value,
            walletId: Number(this.form.get(['walletId']).value),
            syncStatus: this.form.get(['syncStatus']).value,
            spentSharings: this.form.get(['spentSharings']).value,
        };
    }



    async getProfilDataId(profilId: number) {
        let profilResp = await this.profilService.find(profilId).toPromise();
        return profilResp.body.id;
    }

    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }
    compareWallet(first: Wallet, second: Wallet): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWalletById(index: number, item: Wallet) {
        return item.id;
    }

    computeTotalShare() {
        this.totalShare = 0;
        this.spent.spentSharings.forEach(ss => {
            this.totalShare += ss.share;
        });
    }

    amountChange($event) {
        let amount;
        if ($event) {
            amount = $event.target.value;
            this.spent.amount = amount;
        } else {
            amount = this.spent.amount;
        }
        
        this.spent.spentSharings.forEach(ss => {
            ss.amountShare = amount * ss.share / this.totalShare;
        });
        this.form.patchValue({
            amount: amount,
            spentSharings: this.spent.spentSharings
        });
    }

    amountSharingChange($event, index) {
        this.spent.spentSharings[index].share = Number($event.target.value);
        const amount = this.spent.amount;
        this.computeTotalShare();
        this.spent.spentSharings.forEach(ss => {
            ss.amountShare = amount * ss.share / this.totalShare;
        });
        if ($event.target.value != '') {
            this.form.patchValue({
                spentSharings: this.spent.spentSharings
            });
        }
    }
    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.spent.label + '?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.spentService.delete(this.spent.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/wallet/' + this.spent.walletId + '/view');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }

    resetSpentSharingShare(index: number) {
        this.setSpentSharingShare(index, 0);
    }

    addSpentSharingShare(index: number) {
        this.setSpentSharingShare(index, 1);
    }

    setSpentSharingShare(index: number, value:number) {
        this.spent.spentSharings[index].share = value;
        this.form.patchValue({
            spentSharings: this.spent.spentSharings
        });
    }

    spentConfigSelected(event) {
        let spentConfig = event.detail.value;
        this.spentSharingConfigService.findWhereParent([spentConfig]).subscribe(sscsResponse => {
            let spentSharingConfigs = sscsResponse.body;
            for (let spentSharingConfig of spentSharingConfigs) {
                const spentSharing = this.spent.spentSharings.find(ss => ss.sharingProfilId == spentSharingConfig.profilId);
                spentSharing.share = spentSharingConfig.share;
            }
            this.form.patchValue({
                spentSharings: this.spent.spentSharings
            });
        });
        
    }
}
