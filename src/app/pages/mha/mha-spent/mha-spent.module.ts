import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaProfilDataComponentModule } from '../../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../../components/mha/mha-profil/mha-profile.component.module';
import { Spent } from '../../entities/spent';
import { Task } from '../../entities/task';
import { MhaSpentUpdatePage } from './mha-spent-update';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { ProfilService } from '../../entities/profil';


@Injectable({ providedIn: 'root' })
export class MhaSpentResolve implements Resolve<Spent> {
  constructor(private service:MhaSpentService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Spent> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Task>) => response.ok),
        map((task: HttpResponse<Task>) => task.body)
      );
    }
    return of(new Spent());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentFromWalletResolve implements Resolve<Spent> {
  constructor(protected profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Spent> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    const profilId = this.profilService.getStoredProfil().id;
    if (walletId) {
      let spent = new Spent();
      spent.walletId = walletId;
      spent.spenderId = profilId;
      return of(spent);

    }
    return of(new Spent());
  }
}

const routes: Routes = [
  {
    path: ':id/edit',
    component: MhaSpentUpdatePage,
    resolve: {
      data: MhaSpentResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':walletId/new',
    component: MhaSpentUpdatePage,
    resolve: {
      data: MhaSpentFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaSpentUpdatePage],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaSpentPageModule { }
