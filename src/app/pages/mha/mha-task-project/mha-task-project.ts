import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { TaskProject } from '../../entities/task-project';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaProfilDataService } from '../../../services/mha/mha-profil-data-service/mha-profil-data-service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';


@Component({
  selector: 'app-mha-task-project',
  templateUrl: './mha-task-project.html',
  styleUrls: ['./mha-task-project.scss'],
})
export class MhaTaskProjectPage {

  taskProjects: TaskProject[];

  constructor(
    private taskProjectService: MhaTaskProjectService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilDataService: MhaProfilDataService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform) { }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh();
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.taskProjectService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<TaskProject[]>) => res.ok),
      map((res: HttpResponse<TaskProject[]>) => res.body)
    )
      .subscribe(
        (response: TaskProject[]) => {
          this.taskProjects = response;
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: TaskProject) {
    return item.id;
  }
  view(taskProject: TaskProject) {
    this.navController.navigateForward('/menu/task-project/' + taskProject.id + '/view');
  }

  newTaskProject() {
    this.navController.navigateForward('/menu/task-project/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }

  


}
