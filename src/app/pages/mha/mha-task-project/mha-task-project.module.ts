import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MhaTaskComponentModule } from 'src/app/components/mha/mha-task/mha-task.component.module';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaProfilDataComponentModule } from '../../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';
import { TaskProject } from '../../entities/task-project';
import { MhaProfileComponentModule } from '../../../components/mha/mha-profil/mha-profile.component.module';
import { MhaTaskProjectPage } from './mha-task-project';
import { MhaTaskProjectDetailPage } from './mha-task-project-detail';
import { MhaTaskProjectUpdatePage } from './mha-task-project-update';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';



@Injectable({ providedIn: 'root' })
export class MhaTaskProjectResolve implements Resolve<TaskProject> {
  constructor(private service: MhaTaskProjectService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TaskProject> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TaskProject>) => response.ok),
        map((taskProject: HttpResponse<TaskProject>) => taskProject.body)
      );
    }
    return of(new TaskProject());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTaskProjectFromWorkspaceResolve implements Resolve<TaskProject> {
  constructor(private profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TaskProject> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let taskProject = new TaskProject();
      taskProject.workspaceId = workspaceId;
      taskProject.owners = [this.profilService.getStoredProfil()];
      return of(taskProject);

    }
    return of(new TaskProject());
  }
}

const routes: Routes = [
  {
    path: '',
    component: MhaTaskProjectPage
  },
  {
    path: ':id/view',
    component: MhaTaskProjectDetailPage,
    resolve: {
      data: MhaTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MhaTaskProjectUpdatePage,
    resolve: {
      data: MhaTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':workspaceId/new',
    component: MhaTaskProjectUpdatePage,
    resolve: {
      data: MhaTaskProjectFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaTaskComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaTaskProjectPage, MhaTaskProjectDetailPage, MhaTaskProjectUpdatePage],
  entryComponents: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MhaTaskProjectPageModule {}
