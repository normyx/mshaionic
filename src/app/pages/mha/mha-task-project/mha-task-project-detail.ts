import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { JhiDataUtils } from 'ng-jhipster';
import { filter, map } from 'rxjs/operators';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { Task } from '../../entities/task';
import { TaskProject } from '../../entities/task-project';

@Component({
    selector: 'mha-page-task-project-detail',
    templateUrl: 'mha-task-project-detail.html',
    styleUrls: ['./mha-task-project.scss'],
})
export class MhaTaskProjectDetailPage implements OnInit {

    taskProject: TaskProject;
    tasks: Task[];

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private taskService: MhaTaskService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        private dataUtils: JhiDataUtils,

        public popoverController: PopoverController
    ) { }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh();
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.activatedRoute.data.subscribe((response) => {
            this.taskProject = response.data;
            this.taskService.findWhereParent([this.taskProject]).pipe(
                filter((res: HttpResponse<Task[]>) => res.ok),
                map((res: HttpResponse<Task[]>) => res.body))
                .subscribe(
                    (response: Task[]) => {
                        this.tasks = response.sort((a: Task, b: Task) => {
                            if (a.done == b.done) {
                                return moment(b.dueDate).diff(moment(a.dueDate));
                            } else {
                                return a.done?1:-1;
                            }});
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    newTask() {
        this.navController.navigateForward('/menu/task/' + this.taskProject.id + '/new');
    }

    goBack() {
        this.navController.navigateRoot('/menu/task-project');
    }

   
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    trackId(index: number, item: Task) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/task-project/' + this.taskProject.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
      }

   

    


}
