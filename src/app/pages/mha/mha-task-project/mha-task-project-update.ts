import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController, PopoverController, AlertController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TaskProject, TaskProjectService } from '../../entities/task-project';
import { Workspace, WorkspaceService } from '../../entities/workspace';
import { Profil } from '../../entities/profil';
import { MhaProfileChooserPopoverComponent } from '../../../components/mha/mha-profil/mha-profile-chooser-popover.component/mha-profile-chooser-popover.component';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';

@Component({
    selector: 'mha-page-task-project-update',
    templateUrl: 'mha-task-project-update.html'
})
export class MhaTaskProjectUpdatePage implements OnInit {

    taskProject: TaskProject;
    workspace: Workspace;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
          workspaceId: [null, []],
          owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController, 
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private workspaceService: WorkspaceService,
        private taskProjectService: MhaTaskProjectService,
        private popoverCtrl: PopoverController
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.taskProject = response.data;
            this.isNew = this.taskProject.id === null || this.taskProject.id === undefined;
        });
    }

    updateForm(taskProject: TaskProject) {
        this.form.patchValue({
            id: taskProject.id,
            label: taskProject.label,
            workspaceId: taskProject.workspaceId,
            owners: taskProject.owners,
        });
        this.workspaceService.find(taskProject.workspaceId).subscribe(data => {
            this.workspace = data.body;
        }, (error) => this.onError(error));
    }

    save() {
        this.isSaving = true;
        const taskProject = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.taskProjectService.update(taskProject));
        } else {
            this.subscribeToSaveResponse(this.taskProjectService.create(taskProject));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<TaskProject>>) {
        result.subscribe((res: HttpResponse<TaskProject>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
          this.taskProject = response.body;
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `TaskProject ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateForward('/menu/task-project/'+this.taskProject.id+'/view');
    }


    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): TaskProject {
        return {
            ...new TaskProject(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
            syncStatus: this.taskProject.syncStatus
        };
    }

    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.taskProject.owners) {
            this.taskProject.owners = new Array<Profil>();
        }
        for (let workspaceProfil of this.workspace.owners) {
            if (!this.taskProject.owners.some(e => e.id === workspaceProfil.id)) {
                profils.push(workspaceProfil);
            }
        }
        return profils;

    }
    async chooseProfile(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: MhaProfileChooserPopoverComponent,
            event: ev,
            animated: true,
            showBackdrop: true,
            componentProps: { "profils": this.taskProject.owners, "availableProfils": this.getAvailableProfils(), "canBeWithNoActor": false }
        });
        popover.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.taskProject.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.taskProject.owners });
            }
        });
        return await popover.present();
    }

    async deleteModal() {
        const alert = await this.alertController.create({
          header: 'Confirmer vous la suppression de ' + this.taskProject.label + ' ? Toutes les tâches correspondantes seront également supprimées.',
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary'
            }, {
              text: 'Supprimer',
              handler: () => {
                this.taskProjectService.delete(this.taskProject.id).subscribe(() => {
                  this.navController.navigateRoot('/menu/task-project');
                  
                });
    
              }
            }
          ]
        });
        await alert.present();
      }
}
