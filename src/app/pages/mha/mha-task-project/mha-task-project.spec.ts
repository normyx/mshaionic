import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhaTaskProjectPage } from './mha-task-project';

describe('MhaTaskProjectPage', () => {
  let component: MhaTaskProjectPage;
  let fixture: ComponentFixture<MhaTaskProjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhaTaskProjectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhaTaskProjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
