import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { EntitiesPage } from './entities.page';

const routes: Routes = [
  {
    path: '',
    component: EntitiesPage,
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
  , {
    path: 'profil',
    loadChildren: './profil/profil.module#ProfilPageModule'
  }

  , {
    path: 'task',
    loadChildren: './task/task.module#TaskPageModule'
  }
  , {
    path: 'task-project',
    loadChildren: './task-project/task-project.module#TaskProjectPageModule'
  }
  , {
    path: 'workspace',
    loadChildren: './workspace/workspace.module#WorkspacePageModule'
  }
  , {
    path: 'profil-data',
    loadChildren: './profil-data/profil-data.module#ProfilDataPageModule'
  }
  , {
    path: 'wallet',
    loadChildren: './wallet/wallet.module#WalletPageModule'
  }
  , {
    path: 'spent',
    loadChildren: './spent/spent.module#SpentPageModule'
  }
  , {
    path: 'spent-sharing',
    loadChildren: './spent-sharing/spent-sharing.module#SpentSharingPageModule'
  }
  , {
    path: 'spent-sharing-config',
    loadChildren: './spent-sharing-config/spent-sharing-config.module#SpentSharingConfigPageModule'
  }
  , {
    path: 'spent-config',
    loadChildren: './spent-config/spent-config.module#SpentConfigPageModule'
  }
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, RouterModule.forChild(routes), TranslateModule],
  declarations: [EntitiesPage]
})
export class EntitiesPageModule {}
