import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Profil } from './profil.model';

@Injectable({ providedIn: 'root'})
export class ProfilService {
    private resourceUrl = ApiService.API_URL + '/profils';

    constructor(protected http: HttpClient) { }

    create(profil: Profil): Observable<HttpResponse<Profil>> {
        return this.http.post<Profil>(this.resourceUrl, profil, { observe: 'response'});
    }

    update(profil: Profil): Observable<HttpResponse<Profil>> {
        return this.http.put(this.resourceUrl, profil, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<Profil>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<Profil[]>> {
        const options = createRequestOption(req);
        return this.http.get<Profil[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
