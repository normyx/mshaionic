import { Component, OnInit } from '@angular/core';
import { Profil } from './profil.model';
import { ProfilService } from './profil.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-profil-detail',
    templateUrl: 'profil-detail.html'
})
export class ProfilDetailPage implements OnInit {
    profil: Profil;

    constructor(
        private navController: NavController,
        private profilService: ProfilService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.profil = response.data;
        });
    }

    open(item: Profil) {
        this.navController.navigateForward('/tabs/entities/profil/' + item.id + '/edit');
    }

    async deleteModal(item: Profil) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.profilService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/profil');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
