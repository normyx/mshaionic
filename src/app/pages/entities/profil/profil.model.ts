import { BaseEntity } from 'src/model/base-entity';
import { TaskProject } from '../task-project/task-project.model';
import { Workspace } from '../workspace/workspace.model';
import { Task } from '../task/task.model';
import { Wallet } from '../wallet/wallet.model';

export class Profil implements BaseEntity {
    constructor(
        public id?: number,
        public displayName?: string,
        public userLogin?: string,
        public userId?: number,
        public taskProjects?: TaskProject[],
        public workspaces?: Workspace[],
        public tasks?: Task[],
        public profilDataId?: number,
        public wallets?: Wallet[],
    ) {
    }
}
