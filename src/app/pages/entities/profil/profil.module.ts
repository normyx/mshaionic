import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { ProfilPage } from './profil';
import { ProfilUpdatePage } from './profil-update';
import { Profil, ProfilService, ProfilDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class ProfilResolve implements Resolve<Profil> {
  constructor(private service: ProfilService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Profil> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Profil>) => response.ok),
        map((profil: HttpResponse<Profil>) => profil.body)
      );
    }
    return of(new Profil());
  }
}

const routes: Routes = [
    {
      path: '',
      component: ProfilPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: ProfilUpdatePage,
      resolve: {
        data: ProfilResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: ProfilDetailPage,
      resolve: {
        data: ProfilResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: ProfilUpdatePage,
      resolve: {
        data: ProfilResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        ProfilPage,
        ProfilUpdatePage,
        ProfilDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class ProfilPageModule {
}
