import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { SpentSharingPage } from './spent-sharing';
import { SpentSharingUpdatePage } from './spent-sharing-update';
import { SpentSharing, SpentSharingService, SpentSharingDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class SpentSharingResolve implements Resolve<SpentSharing> {
  constructor(private service: SpentSharingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpentSharing> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentSharing>) => response.ok),
        map((spentSharing: HttpResponse<SpentSharing>) => spentSharing.body)
      );
    }
    return of(new SpentSharing());
  }
}

const routes: Routes = [
    {
      path: '',
      component: SpentSharingPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: SpentSharingUpdatePage,
      resolve: {
        data: SpentSharingResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: SpentSharingDetailPage,
      resolve: {
        data: SpentSharingResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: SpentSharingUpdatePage,
      resolve: {
        data: SpentSharingResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        SpentSharingPage,
        SpentSharingUpdatePage,
        SpentSharingDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class SpentSharingPageModule {
}
