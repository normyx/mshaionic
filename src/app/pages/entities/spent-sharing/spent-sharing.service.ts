import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { SpentSharing } from './spent-sharing.model';

@Injectable({ providedIn: 'root'})
export class SpentSharingService {
    private resourceUrl = ApiService.API_URL + '/spent-sharings';

    constructor(protected http: HttpClient) { }

    create(spentSharing: SpentSharing): Observable<HttpResponse<SpentSharing>> {
        return this.http.post<SpentSharing>(this.resourceUrl, spentSharing, { observe: 'response'});
    }

    update(spentSharing: SpentSharing): Observable<HttpResponse<SpentSharing>> {
        return this.http.put(this.resourceUrl, spentSharing, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<SpentSharing>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<SpentSharing[]>> {
        const options = createRequestOption(req);
        return this.http.get<SpentSharing[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
