import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SpentSharing } from './spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { Profil, ProfilService } from '../profil';
import { Spent, SpentService } from '../spent';

@Component({
    selector: 'page-spent-sharing-update',
    templateUrl: 'spent-sharing-update.html'
})
export class SpentSharingUpdatePage implements OnInit {

    spentSharing: SpentSharing;
    profils: Profil[];
    spents: Spent[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        amountShare: [null, [Validators.required]],
        share: [null, [Validators.required]],
          sharingProfilId: [null, []],
          spentId: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private profilService: ProfilService,
        private spentService: SpentService,
        private spentSharingService: SpentSharingService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.profilService.query()
            .subscribe(data => { this.profils = data.body; }, (error) => this.onError(error));
        this.spentService.query()
            .subscribe(data => { this.spents = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.spentSharing = response.data;
            this.isNew = this.spentSharing.id === null || this.spentSharing.id === undefined;
        });
    }

    updateForm(spentSharing: SpentSharing) {
        this.form.patchValue({
            id: spentSharing.id,
            amountShare: spentSharing.amountShare,
            share: spentSharing.share,
            sharingProfilId: spentSharing.sharingProfilId,
            spentId: spentSharing.spentId,
        });
    }

    save() {
        this.isSaving = true;
        const spentSharing = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.spentSharingService.update(spentSharing));
        } else {
            this.subscribeToSaveResponse(this.spentSharingService.create(spentSharing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<SpentSharing>>) {
        result.subscribe((res: HttpResponse<SpentSharing>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `SpentSharing ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/spent-sharing');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): SpentSharing {
        return {
            ...new SpentSharing(),
            id: this.form.get(['id']).value,
            amountShare: this.form.get(['amountShare']).value,
            share: this.form.get(['share']).value,
            sharingProfilId: this.form.get(['sharingProfilId']).value,
            spentId: this.form.get(['spentId']).value,
        };
    }

    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }
    compareSpent(first: Spent, second: Spent): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackSpentById(index: number, item: Spent) {
        return item.id;
    }
}
