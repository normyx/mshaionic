import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { SpentSharing } from './spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';

@Component({
    selector: 'page-spent-sharing',
    templateUrl: 'spent-sharing.html'
})
export class SpentSharingPage {
    spentSharings: SpentSharing[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private spentSharingService: SpentSharingService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.spentSharings = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.spentSharingService.query().pipe(
            filter((res: HttpResponse<SpentSharing[]>) => res.ok),
            map((res: HttpResponse<SpentSharing[]>) => res.body)
        )
        .subscribe(
            (response: SpentSharing[]) => {
                this.spentSharings = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: SpentSharing) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/spent-sharing/new');
    }

    edit(item: IonItemSliding, spentSharing: SpentSharing) {
        this.navController.navigateForward('/tabs/entities/spent-sharing/' + spentSharing.id + '/edit');
        item.close();
    }

    async delete(spentSharing) {
        this.spentSharingService.delete(spentSharing.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'SpentSharing deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(spentSharing: SpentSharing) {
        this.navController.navigateForward('/tabs/entities/spent-sharing/' + spentSharing.id + '/view');
    }
}
