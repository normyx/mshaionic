import { Component, OnInit } from '@angular/core';
import { SpentSharing } from './spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-spent-sharing-detail',
    templateUrl: 'spent-sharing-detail.html'
})
export class SpentSharingDetailPage implements OnInit {
    spentSharing: SpentSharing;

    constructor(
        private navController: NavController,
        private spentSharingService: SpentSharingService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.spentSharing = response.data;
        });
    }

    open(item: SpentSharing) {
        this.navController.navigateForward('/tabs/entities/spent-sharing/' + item.id + '/edit');
    }

    async deleteModal(item: SpentSharing) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.spentSharingService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/spent-sharing');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
