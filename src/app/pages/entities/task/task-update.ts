import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Task } from './task.model';
import { TaskService } from './task.service';
import { TaskProject, TaskProjectService } from '../task-project';
import { Profil, ProfilService } from '../profil';

@Component({
    selector: 'page-task-update',
    templateUrl: 'task-update.html'
})
export class TaskUpdatePage implements OnInit {

    task: Task;
    taskProjects: TaskProject[];
    profils: Profil[];
    dueDateDp: any;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        description: [null, []],
        done: ['false', [Validators.required]],
        dueDate: [null, []],
        priority: [null, [Validators.required]],
          taskProjectId: [null, []],
          owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private taskProjectService: TaskProjectService,
        private profilService: ProfilService,
        private taskService: TaskService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.taskProjectService.query()
            .subscribe(data => { this.taskProjects = data.body; }, (error) => this.onError(error));
        this.profilService.query()
            .subscribe(data => { this.profils = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.task = response.data;
            this.isNew = this.task.id === null || this.task.id === undefined;
        });
    }

    updateForm(task: Task) {
        this.form.patchValue({
            id: task.id,
            label: task.label,
            description: task.description,
            done: task.done,
            dueDate: task.dueDate,
            priority: task.priority,
            taskProjectId: task.taskProjectId,
            owners: task.owners,
        });
    }

    save() {
        this.isSaving = true;
        const task = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.taskService.update(task));
        } else {
            this.subscribeToSaveResponse(this.taskService.create(task));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Task>>) {
        result.subscribe((res: HttpResponse<Task>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Task ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/task');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Task {
        return {
            ...new Task(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            description: this.form.get(['description']).value,
            done: this.form.get(['done']).value,
            dueDate: this.form.get(['dueDate']).value,
            priority: this.form.get(['priority']).value,
            taskProjectId: this.form.get(['taskProjectId']).value,
            owners: this.form.get(['owners']).value,
        };
    }

    compareTaskProject(first: TaskProject, second: TaskProject): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackTaskProjectById(index: number, item: TaskProject) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }
}
