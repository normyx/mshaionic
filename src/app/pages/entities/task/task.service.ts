import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Task } from './task.model';

type EntityResponseType = HttpResponse<Task>;
type EntityArrayResponseType = HttpResponse<Task[]>;

@Injectable({ providedIn: 'root' })
export class TaskService {
    private resourceUrl = ApiService.API_URL + '/tasks';

    constructor(protected http: HttpClient) { }

    create(task: Task): Observable<EntityResponseType> {
        return this.http.post<Task>(this.resourceUrl, this.convertDateFromClient(task), { observe: 'response' });
    }

    update(task: Task): Observable<EntityResponseType> {
        return this.http.put(this.resourceUrl, this.convertDateFromClient(task), { observe: 'response' });
    }

    find(id: number): Observable<HttpResponse<Task>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<Task[]>> {
        const options = createRequestOption(req);
        return this.http.get<Task[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(task: Task): Task {
        const copy: Task = Object.assign({}, task, {
            dueDate: task.dueDate != null ? moment(task.dueDate).format("YYYY-MM-DD") : null
        });
        return copy;
    }

}
