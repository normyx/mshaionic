import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Task } from './task.model';
import { TaskService } from './task.service';

@Component({
    selector: 'page-task',
    templateUrl: 'task.html'
})
export class TaskPage {
    tasks: Task[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private taskService: TaskService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.tasks = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.taskService.query().pipe(
            filter((res: HttpResponse<Task[]>) => res.ok),
            map((res: HttpResponse<Task[]>) => res.body)
        )
        .subscribe(
            (response: Task[]) => {
                this.tasks = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Task) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/task/new');
    }

    edit(item: IonItemSliding, task: Task) {
        this.navController.navigateForward('/tabs/entities/task/' + task.id + '/edit');
        item.close();
    }

    async delete(task) {
        this.taskService.delete(task.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Task deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(task: Task) {
        this.navController.navigateForward('/tabs/entities/task/' + task.id + '/view');
    }
}
