import { Component, OnInit } from '@angular/core';
import { Task } from './task.model';
import { TaskService } from './task.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-task-detail',
    templateUrl: 'task-detail.html'
})
export class TaskDetailPage implements OnInit {
    task: Task;

    constructor(
        private navController: NavController,
        private taskService: TaskService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.task = response.data;
        });
    }

    open(item: Task) {
        this.navController.navigateForward('/tabs/entities/task/' + item.id + '/edit');
    }

    async deleteModal(item: Task) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.taskService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/task');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
