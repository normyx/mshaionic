import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Profil } from '../profil/profil.model';

export class Task implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public description?: string,
        public done?: boolean,
        public dueDate?: any,
        public priority?: number,
        public taskProjectLabel?: string,
        public taskProjectId?: number,
        public owners?: Profil[],
        public syncStatus?: SyncStatus
    ) {
        this.done = false;
    }
}
