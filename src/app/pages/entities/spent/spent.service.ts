import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Spent } from './spent.model';

@Injectable({ providedIn: 'root'})
export class SpentService {
    private resourceUrl = ApiService.API_URL + '/spents';

    constructor(protected http: HttpClient) { }

    create(spent: Spent): Observable<HttpResponse<Spent>> {
        return this.http.post<Spent>(this.resourceUrl, this.convertDateFromClient(spent), { observe: 'response'});
    }

    update(spent: Spent): Observable<HttpResponse<Spent>> {
        return this.http.put(this.resourceUrl, this.convertDateFromClient(spent), { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<Spent>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<Spent[]>> {
        const options = createRequestOption(req);
        return this.http.get<Spent[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    protected convertDateFromClient(spent: Spent): Spent {
        const copy: Spent = Object.assign({}, spent, {
            spentDate: spent.spentDate != null ? moment(spent.spentDate).format("YYYY-MM-DD") : null,
            lastUpdateDate: spent.lastUpdateDate != null ? moment(spent.lastUpdateDate).format("YYYY-MM-DD") : null,
        });
        return copy;
    }
}
