import { Component, OnInit } from '@angular/core';
import { Spent } from './spent.model';
import { SpentService } from './spent.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-spent-detail',
    templateUrl: 'spent-detail.html'
})
export class SpentDetailPage implements OnInit {
    spent: Spent;

    constructor(
        private navController: NavController,
        private spentService: SpentService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.spent = response.data;
        });
    }

    open(item: Spent) {
        this.navController.navigateForward('/tabs/entities/spent/' + item.id + '/edit');
    }

    async deleteModal(item: Spent) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.spentService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/spent');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
