import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Wallet } from './wallet.model';
import { WalletService } from './wallet.service';

@Component({
    selector: 'page-wallet',
    templateUrl: 'wallet.html'
})
export class WalletPage {
    wallets: Wallet[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private walletService: WalletService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.wallets = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.walletService.query().pipe(
            filter((res: HttpResponse<Wallet[]>) => res.ok),
            map((res: HttpResponse<Wallet[]>) => res.body)
        )
        .subscribe(
            (response: Wallet[]) => {
                this.wallets = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Wallet) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/wallet/new');
    }

    edit(item: IonItemSliding, wallet: Wallet) {
        this.navController.navigateForward('/tabs/entities/wallet/' + wallet.id + '/edit');
        item.close();
    }

    async delete(wallet) {
        this.walletService.delete(wallet.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Wallet deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(wallet: Wallet) {
        this.navController.navigateForward('/tabs/entities/wallet/' + wallet.id + '/view');
    }
}
