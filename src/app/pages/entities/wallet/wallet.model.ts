import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Profil } from '../profil/profil.model';

export class Wallet implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public workspaceLabel?: string,
        public workspaceId?: number,
        public owners?: Profil[],
        public syncStatus?: SyncStatus
    ) {
    }
}
