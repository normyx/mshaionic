import { Component, OnInit } from '@angular/core';
import { Wallet } from './wallet.model';
import { WalletService } from './wallet.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-wallet-detail',
    templateUrl: 'wallet-detail.html'
})
export class WalletDetailPage implements OnInit {
    wallet: Wallet;

    constructor(
        private navController: NavController,
        private walletService: WalletService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.wallet = response.data;
        });
    }

    open(item: Wallet) {
        this.navController.navigateForward('/tabs/entities/wallet/' + item.id + '/edit');
    }

    async deleteModal(item: Wallet) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.walletService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/wallet');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
