import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Workspace } from './workspace.model';
import { WorkspaceService } from './workspace.service';
import { Profil, ProfilService } from '../profil';

@Component({
    selector: 'page-workspace-update',
    templateUrl: 'workspace-update.html'
})
export class WorkspaceUpdatePage implements OnInit {

    workspace: Workspace;
    profils: Profil[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
          owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private profilService: ProfilService,
        private workspaceService: WorkspaceService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.profilService.query()
            .subscribe(data => { this.profils = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.workspace = response.data;
            this.isNew = this.workspace.id === null || this.workspace.id === undefined;
        });
    }

    updateForm(workspace: Workspace) {
        this.form.patchValue({
            id: workspace.id,
            label: workspace.label,
            owners: workspace.owners,
        });
    }

    save() {
        this.isSaving = true;
        const workspace = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.workspaceService.update(workspace));
        } else {
            this.subscribeToSaveResponse(this.workspaceService.create(workspace));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Workspace>>) {
        result.subscribe((res: HttpResponse<Workspace>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Workspace ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/workspace');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Workspace {
        return {
            ...new Workspace(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            owners: this.form.get(['owners']).value,
        };
    }

    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }
}
