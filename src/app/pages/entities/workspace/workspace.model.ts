import { BaseEntity } from 'src/model/base-entity';
import { Profil } from '../profil/profil.model';
import { TaskProject } from '../task-project/task-project.model';
import { Wallet } from '../wallet/wallet.model';

export class Workspace implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public owners?: Profil[],
        public taskProjects?: TaskProject[],
        public wallets?: Wallet[],
    ) {
    }
}
