import { Component, OnInit } from '@angular/core';
import { Workspace } from './workspace.model';
import { WorkspaceService } from './workspace.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-workspace-detail',
    templateUrl: 'workspace-detail.html'
})
export class WorkspaceDetailPage implements OnInit {
    workspace: Workspace;

    constructor(
        private navController: NavController,
        private workspaceService: WorkspaceService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.workspace = response.data;
        });
    }

    open(item: Workspace) {
        this.navController.navigateForward('/tabs/entities/workspace/' + item.id + '/edit');
    }

    async deleteModal(item: Workspace) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.workspaceService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/workspace');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
