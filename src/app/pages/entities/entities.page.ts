import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-entities',
  templateUrl: 'entities.page.html',
  styleUrls: ['entities.page.scss']
})
export class EntitiesPage {
  entities: Array<any> = [
    {name: 'Profil', component: 'ProfilPage', route: 'profil'},
    {name: 'Task', component: 'TaskPage', route: 'task'},
    {name: 'TaskProject', component: 'TaskProjectPage', route: 'task-project'},
    {name: 'Workspace', component: 'WorkspacePage', route: 'workspace'},
    {name: 'ProfilData', component: 'ProfilDataPage', route: 'profil-data'},
    {name: 'Wallet', component: 'WalletPage', route: 'wallet'},
    {name: 'Spent', component: 'SpentPage', route: 'spent'},
    {name: 'SpentSharing', component: 'SpentSharingPage', route: 'spent-sharing'},
    {name: 'SpentSharingConfig', component: 'SpentSharingConfigPage', route: 'spent-sharing-config'},
    {name: 'SpentConfig', component: 'SpentConfigPage', route: 'spent-config'},
    /* jhipster-needle-add-entity-page - JHipster will add entity pages here */
  ];

  constructor(public navController: NavController) {}

  openPage(page) {
    this.navController.navigateForward('/tabs/entities/' + page.route);
  }

}
