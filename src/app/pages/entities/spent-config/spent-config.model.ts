import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { SpentSharingConfig } from '../spent-sharing-config';

export class SpentConfig implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public walletLabel?: string,
        public walletId?: number,
        public spentSharingConfigs?: SpentSharingConfig[],
        public syncStatus?: SyncStatus
    ) {
    }
}
