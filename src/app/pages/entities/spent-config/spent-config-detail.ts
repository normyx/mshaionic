import { Component, OnInit } from '@angular/core';
import { SpentConfig } from './spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-spent-config-detail',
    templateUrl: 'spent-config-detail.html'
})
export class SpentConfigDetailPage implements OnInit {
    spentConfig: SpentConfig;

    constructor(
        private navController: NavController,
        private spentConfigService: SpentConfigService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.spentConfig = response.data;
        });
    }

    open(item: SpentConfig) {
        this.navController.navigateForward('/tabs/entities/spent-config/' + item.id + '/edit');
    }

    async deleteModal(item: SpentConfig) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.spentConfigService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/spent-config');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
