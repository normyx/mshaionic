import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { SpentConfigPage } from './spent-config';
import { SpentConfigUpdatePage } from './spent-config-update';
import { SpentConfig, SpentConfigService, SpentConfigDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class SpentConfigResolve implements Resolve<SpentConfig> {
  constructor(private service: SpentConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpentConfig> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentConfig>) => response.ok),
        map((spentConfig: HttpResponse<SpentConfig>) => spentConfig.body)
      );
    }
    return of(new SpentConfig());
  }
}

const routes: Routes = [
    {
      path: '',
      component: SpentConfigPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: SpentConfigUpdatePage,
      resolve: {
        data: SpentConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: SpentConfigDetailPage,
      resolve: {
        data: SpentConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: SpentConfigUpdatePage,
      resolve: {
        data: SpentConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        SpentConfigPage,
        SpentConfigUpdatePage,
        SpentConfigDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class SpentConfigPageModule {
}
