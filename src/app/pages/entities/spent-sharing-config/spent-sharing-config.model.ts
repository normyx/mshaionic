import { BaseEntity } from 'src/model/base-entity';

export class SpentSharingConfig implements BaseEntity {
    constructor(
        public id?: number,
        public share?: number,
        public spentConfigLabel?: string,
        public spentConfigId?: number,
        public profilDisplayName?: string,
        public profilId?: number,
    ) {
    }
}
