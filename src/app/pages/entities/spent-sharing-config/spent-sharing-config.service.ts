import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { SpentSharingConfig } from './spent-sharing-config.model';

@Injectable({ providedIn: 'root'})
export class SpentSharingConfigService {
    private resourceUrl = ApiService.API_URL + '/spent-sharing-configs';

    constructor(protected http: HttpClient) { }

    create(spentSharingConfig: SpentSharingConfig): Observable<HttpResponse<SpentSharingConfig>> {
        return this.http.post<SpentSharingConfig>(this.resourceUrl, spentSharingConfig, { observe: 'response'});
    }

    update(spentSharingConfig: SpentSharingConfig): Observable<HttpResponse<SpentSharingConfig>> {
        return this.http.put(this.resourceUrl, spentSharingConfig, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<SpentSharingConfig>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<SpentSharingConfig[]>> {
        const options = createRequestOption(req);
        return this.http.get<SpentSharingConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
