import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { SpentSharingConfigPage } from './spent-sharing-config';
import { SpentSharingConfigUpdatePage } from './spent-sharing-config-update';
import { SpentSharingConfig, SpentSharingConfigService, SpentSharingConfigDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class SpentSharingConfigResolve implements Resolve<SpentSharingConfig> {
  constructor(private service: SpentSharingConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpentSharingConfig> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentSharingConfig>) => response.ok),
        map((spentSharingConfig: HttpResponse<SpentSharingConfig>) => spentSharingConfig.body)
      );
    }
    return of(new SpentSharingConfig());
  }
}

const routes: Routes = [
    {
      path: '',
      component: SpentSharingConfigPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: SpentSharingConfigUpdatePage,
      resolve: {
        data: SpentSharingConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: SpentSharingConfigDetailPage,
      resolve: {
        data: SpentSharingConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: SpentSharingConfigUpdatePage,
      resolve: {
        data: SpentSharingConfigResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        SpentSharingConfigPage,
        SpentSharingConfigUpdatePage,
        SpentSharingConfigDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class SpentSharingConfigPageModule {
}
