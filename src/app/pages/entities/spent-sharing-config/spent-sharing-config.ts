import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { SpentSharingConfig } from './spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';

@Component({
    selector: 'page-spent-sharing-config',
    templateUrl: 'spent-sharing-config.html'
})
export class SpentSharingConfigPage {
    spentSharingConfigs: SpentSharingConfig[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private spentSharingConfigService: SpentSharingConfigService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.spentSharingConfigs = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.spentSharingConfigService.query().pipe(
            filter((res: HttpResponse<SpentSharingConfig[]>) => res.ok),
            map((res: HttpResponse<SpentSharingConfig[]>) => res.body)
        )
        .subscribe(
            (response: SpentSharingConfig[]) => {
                this.spentSharingConfigs = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: SpentSharingConfig) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/spent-sharing-config/new');
    }

    edit(item: IonItemSliding, spentSharingConfig: SpentSharingConfig) {
        this.navController.navigateForward('/tabs/entities/spent-sharing-config/' + spentSharingConfig.id + '/edit');
        item.close();
    }

    async delete(spentSharingConfig) {
        this.spentSharingConfigService.delete(spentSharingConfig.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'SpentSharingConfig deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(spentSharingConfig: SpentSharingConfig) {
        this.navController.navigateForward('/tabs/entities/spent-sharing-config/' + spentSharingConfig.id + '/view');
    }
}
