import { Component, OnInit } from '@angular/core';
import { SpentSharingConfig } from './spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-spent-sharing-config-detail',
    templateUrl: 'spent-sharing-config-detail.html'
})
export class SpentSharingConfigDetailPage implements OnInit {
    spentSharingConfig: SpentSharingConfig;

    constructor(
        private navController: NavController,
        private spentSharingConfigService: SpentSharingConfigService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.spentSharingConfig = response.data;
        });
    }

    open(item: SpentSharingConfig) {
        this.navController.navigateForward('/tabs/entities/spent-sharing-config/' + item.id + '/edit');
    }

    async deleteModal(item: SpentSharingConfig) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.spentSharingConfigService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/spent-sharing-config');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
