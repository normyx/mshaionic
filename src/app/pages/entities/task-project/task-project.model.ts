import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Profil } from '../profil/profil.model';
import { Task } from '../task/task.model';

export class TaskProject implements BaseEntity {
    constructor(
        public id?: number,
        public label?: string,
        public workspaceLabel?: string,
        public workspaceId?: number,
        public owners?: Profil[],
        public tasks?: Task[],
        public syncStatus?: SyncStatus
    ) {
    }
}
