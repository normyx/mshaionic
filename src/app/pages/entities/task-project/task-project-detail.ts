import { Component, OnInit } from '@angular/core';
import { TaskProject } from './task-project.model';
import { TaskProjectService } from './task-project.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-task-project-detail',
    templateUrl: 'task-project-detail.html'
})
export class TaskProjectDetailPage implements OnInit {
    taskProject: TaskProject;

    constructor(
        private navController: NavController,
        private taskProjectService: TaskProjectService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.taskProject = response.data;
        });
    }

    open(item: TaskProject) {
        this.navController.navigateForward('/tabs/entities/task-project/' + item.id + '/edit');
    }

    async deleteModal(item: TaskProject) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.taskProjectService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/task-project');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
