import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TaskProject } from './task-project.model';
import { TaskProjectService } from './task-project.service';

@Component({
    selector: 'page-task-project',
    templateUrl: 'task-project.html'
})
export class TaskProjectPage {
    taskProjects: TaskProject[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private taskProjectService: TaskProjectService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.taskProjects = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.taskProjectService.query().pipe(
            filter((res: HttpResponse<TaskProject[]>) => res.ok),
            map((res: HttpResponse<TaskProject[]>) => res.body)
        )
        .subscribe(
            (response: TaskProject[]) => {
                this.taskProjects = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: TaskProject) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/task-project/new');
    }

    edit(item: IonItemSliding, taskProject: TaskProject) {
        this.navController.navigateForward('/tabs/entities/task-project/' + taskProject.id + '/edit');
        item.close();
    }

    async delete(taskProject) {
        this.taskProjectService.delete(taskProject.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'TaskProject deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(taskProject: TaskProject) {
        this.navController.navigateForward('/tabs/entities/task-project/' + taskProject.id + '/view');
    }
}
