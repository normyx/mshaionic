import { Component, OnInit } from '@angular/core';
import { JhiDataUtils } from 'ng-jhipster';
import { ProfilData } from './profil-data.model';
import { ProfilDataService } from './profil-data.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-profil-data-detail',
    templateUrl: 'profil-data-detail.html'
})
export class ProfilDataDetailPage implements OnInit {
    profilData: ProfilData;

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private profilDataService: ProfilDataService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.profilData = response.data;
        });
    }

    open(item: ProfilData) {
        this.navController.navigateForward('/tabs/entities/profil-data/' + item.id + '/edit');
    }

    async deleteModal(item: ProfilData) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.profilDataService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/profil-data');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

}
