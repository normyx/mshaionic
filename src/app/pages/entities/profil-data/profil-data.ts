import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { JhiDataUtils } from 'ng-jhipster';
import { ProfilData } from './profil-data.model';
import { ProfilDataService } from './profil-data.service';

@Component({
    selector: 'page-profil-data',
    templateUrl: 'profil-data.html'
})
export class ProfilDataPage {
    profilData: ProfilData[];

    // todo: add pagination

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private profilDataService: ProfilDataService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.profilData = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.profilDataService.query().pipe(
            filter((res: HttpResponse<ProfilData[]>) => res.ok),
            map((res: HttpResponse<ProfilData[]>) => res.body)
        )
        .subscribe(
            (response: ProfilData[]) => {
                this.profilData = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: ProfilData) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    new() {
        this.navController.navigateForward('/tabs/entities/profil-data/new');
    }

    edit(item: IonItemSliding, profilData: ProfilData) {
        this.navController.navigateForward('/tabs/entities/profil-data/' + profilData.id + '/edit');
        item.close();
    }

    async delete(profilData) {
        this.profilDataService.delete(profilData.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'ProfilData deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(profilData: ProfilData) {
        this.navController.navigateForward('/tabs/entities/profil-data/' + profilData.id + '/view');
    }
}
