import { BaseEntity } from 'src/model/base-entity';

export class ProfilData implements BaseEntity {
    constructor(
        public id?: number,
        public photoContentType?: string,
        public photo?: any,
    ) {
    }
}
