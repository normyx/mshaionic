import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseEntity } from 'src/model/base-entity';

export interface MhaEntityInterfaceService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> {
    create(entity: T): Observable<HttpResponse<T>>;

    update(entity: T): Observable<HttpResponse<T>>;

    find(id: number): Observable<HttpResponse<T>>;

    query(req?: any): Observable<HttpResponse<T[]>>;

    delete(id: number): Observable<HttpResponse<any>>;

    findWhereParent(parents?: P[], root?: R): Observable<HttpResponse<T[]>>;
}