import { HttpResponse } from '@angular/common/http';
import { EMPTY, Observable, of } from 'rxjs';
import { expand, map } from 'rxjs/operators';
import { BaseEntity } from 'src/model/base-entity';
import { MhaEntityServerInterfaceService } from './mha-entity-server-interface.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})
export class MhaEntityServerRecursiveCallService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> {

    static MAX_NUMBER_OF_PARENT_ELEMENTS = 100;
    static MAX_NUMBER_OF_RETRIEVED_ELEMENTS = 100000;

    constructor() {
    }

    findWhereParent(serverService: MhaEntityServerInterfaceService<T, P, R>, parents: P[], parentQueryStr: string, root?: R, rootStr?:string): Observable<HttpResponse<T[]>> {
        if (!parents || parents.length == 0) return of(new HttpResponse({ body: [] }));
        let parentIds: number[][] = [];
        parentIds.push([]);
        let num = 0;
        for (let parent of parents) {
            if (num == MhaEntityServerRecursiveCallService.MAX_NUMBER_OF_PARENT_ELEMENTS) {
                parentIds.push([]);
                num = 0;
            }
            parentIds[parentIds.length - 1].push(parent.id);
            num++;
        }
        const entities: T[] = [];
        return this.retrieveEntities(serverService, parentIds, parentQueryStr, root, rootStr).pipe(
            map((ret: HttpResponse<T[]>) => {
                for (const ss of ret.body) {
                    entities.push(ss);
                }
                return new HttpResponse({ body: entities });
            })
        )

    }

    private retrieveEntities(serverService: MhaEntityServerInterfaceService<T, P, R>, parentIdIdss: number[][],  parentQueryStr: string, root?: R, rootStr?:string): Observable<HttpResponse<T[]>> {
        let parentIds = parentIdIdss.pop();
        return this.fetchEntities(serverService, parentIds, parentQueryStr, root, rootStr).pipe(
            expand((res: HttpResponse<T[]>) => {
                if (parentIdIdss.length > 0) {
                    return this.retrieveEntities(serverService, parentIdIdss, parentQueryStr, root, rootStr);
                } else {
                    return EMPTY;
                }
            }));
    }

    fetchEntities(serverService: MhaEntityServerInterfaceService<T, P, R>, parentIds: number[],  parentQueryStr: string, root?: R, rootStr?:string): Observable<HttpResponse<T[]>> {
        let queryObj = {};
        queryObj[parentQueryStr] = parentIds;
        queryObj['size'] = MhaEntityServerRecursiveCallService.MAX_NUMBER_OF_RETRIEVED_ELEMENTS;
        if (rootStr && root) {
            queryObj[rootStr] = root.id;
        }
        return serverService.query(queryObj);
    }
}