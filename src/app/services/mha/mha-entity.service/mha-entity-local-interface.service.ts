import { MhaEntityInterfaceService } from './mha-entity-interface.service';
import { SyncStatus, BaseEntity } from 'src/model/base-entity';

export interface MhaEntityLocalInterfaceService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> extends MhaEntityInterfaceService<T,P,R> {
    add(entities: T[]) ;
    remove(entity:T);
    changeParentId(oldParentId:number, newParentId:number);
    //changeId(oldId: number, newId: number);
    clear();
    findWithStatus(status: SyncStatus): T[];
    getNewId(): number;
    sort();
    hasModification(): boolean;
}