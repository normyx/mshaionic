import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { MhaEntityLocalInterfaceService } from './mha-entity-local-interface.service';

export abstract class MhaEntityLocalImplService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> implements MhaEntityLocalInterfaceService<T, P, R> {



    entities: T[];
    newId: number;

    constructor() {


    }

    hasModification(): boolean {
        for (let entity of this.entities) {
            if (entity.syncStatus == SyncStatus.CREATE || entity.syncStatus == SyncStatus.DELETE || entity.syncStatus ==    SyncStatus.UPDATE) return true;
        }
        return false;
    }

    update(entity: T): Observable<HttpResponse<T>> {
        let index = this.entities.findIndex(t => t.id == entity.id);
        if (entity.syncStatus != SyncStatus.CREATE) {
            entity.syncStatus = SyncStatus.UPDATE;
        }
        if (index >= 0) {
            this.entities.splice(index, 1);
            this.entities.push(entity);
            this.entities = this.entities;
            return of(new HttpResponse({ body: entity, status: 200 }));
        } else {
            throw new Error("Entity " + entity + " not found.");
        }

    }
    create(entity: T): Observable<HttpResponse<T>> {
        entity.id = this.getNewId();
        entity.syncStatus = SyncStatus.CREATE;
        this.entities.push(entity);
        this.entities = this.entities;
        return of(new HttpResponse({ body: entity, status: 201 }));
    }

    find(id: number): Observable<HttpResponse<T>> {
        let entity = this.entities.find(t => t.id == id);
        if (entity) {
            return of(new HttpResponse({ body: entity, status: 200 }));
        } else {
            return of(new HttpResponse({ status: 404 }));
        }
    }

    query(req?: any): Observable<HttpResponse<T[]>> { return null; }

    delete(id: number): Observable<HttpResponse<any>> {
        let entity = this.entities.find(t => t.id == id);
        if (entity) {
            let index = this.entities.findIndex(t => t.id == entity.id);
            entity.syncStatus = SyncStatus.DELETE;
            this.entities.splice(index, 1);
            this.entities.push(entity);
            this.entities = this.entities;
            return of(new HttpResponse({}));
        } else {
            return of(new HttpResponse({ status: 404 }));
        }
    }



    public add(entities: T[]) {
        if (this.entities == null) {
            this.clear();
        }
        if (entities) {
            for (let entity of entities) {
                if (this.entities.findIndex(e => { return e.id == entity.id }) == -1) {
                    this.entities.push(entity);
                }
            }
            this.sort();
            this.entities = this.entities;
        }
    }

    public get(index: number): T {
        return this.entities[index];
    }

    public remove(entity: T) {
        if (this.entities) {
            let index = this.entities.findIndex(t => t.id == entity.id);
            this.entities.splice(index, 1);

            this.entities = this.entities;
        }
    }


    public clear() {
        this.entities = [];
        this.newId = -1;
    }

    public findWithStatus(status: SyncStatus): T[] {
        return this.entities ? this.entities.filter(t => t.syncStatus == status) : [];
    }

    public getNewId(): number {
        if (this.newId == null) this.newId = -1;
        let id = this.newId;
        this.newId = this.newId - 1;
        return id;
    }

    abstract changeParentId(oldParentId: number, newParentId: number);
    abstract findWhereParent(parents?: P[], root?: R): Observable<HttpResponse<T[]>>;
    abstract sort();

}