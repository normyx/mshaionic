import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { MhaEntityInterfaceService } from './mha-entity-interface.service';
import { MhaEntityLocalInterfaceService } from './mha-entity-local-interface.service';
import { MhaEntityServerInterfaceService } from './mha-entity-server-interface.service';

export abstract class MhaProxyClassService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> implements MhaEntityInterfaceService<T, P, R> {

    protected LOG_ACTIVATED = false;



    constructor(
        protected localService: MhaEntityLocalInterfaceService<T, P, R>,
        protected serverService: MhaEntityServerInterfaceService<T, P, R>,
        protected childrenProxyService: MhaProxyClassService<any, any, any>[]) {
    }

    private log(logtext: String) {
        if (this.LOG_ACTIVATED) console.warn(this.constructor.name + "::" + logtext);
    }

    create(entity: T): Observable<HttpResponse<T>> { return this.localService.create(entity); }

    update(entity: T): Observable<HttpResponse<T>> { return this.localService.update(entity); }

    find(id: number): Observable<HttpResponse<T>> { return this.localService.find(id); }

    query(req?: any): Observable<HttpResponse<T[]>> { return this.serverService.query(req); }

    delete(id: number): Observable<HttpResponse<any>> { return this.localService.delete(id); }

    hasModification() : boolean {return this.localService.hasModification();}

    findWhereParent(parents?: P[], root?: R): Observable<HttpResponse<T[]>> {
        this.localService.sort();
        return this.localService.findWhereParent(parents, root);
    }

    async commitUpdatedChildrenEntity() {
        this.log("commitUpdatedChildrenEntity()");
        for (let service of this.childrenProxyService) {
            await service.commitUpdatedEntities();
        }
    }

    async commitUpdatedEntities() {
        this.log("commitUpdatedEntities()");
        let entities = this.localService.findWithStatus(SyncStatus.UPDATE);
        for (let entity of entities) {
            this.log("commitUpdatedEntities(): update[" + entity + "]");
            await this.serverService.update(entity).toPromise();
        }
        await this.commitUpdatedChildrenEntity();
    }

    async commitCreatedFromParent(oldParentId: number, parent: P) {
        this.log("commitCreatedFromParent(" + oldParentId + "," + parent.id + ")");
        this.localService.changeParentId(oldParentId, parent.id);
        const entitiesResponse = await this.localService.findWhereParent([parent]).toPromise();
        const entities = entitiesResponse.body.filter(e => e.syncStatus == SyncStatus.CREATE);
        for (let entity of entities) {
            await this.commitCreatedEntity(entity);
        }

    }


    async commitCreatedChildren(oldParentId: number, parent: T) {
        this.log("commitCreatedChildren(" + oldParentId + "," + parent.id + ")");
        for (let service of this.childrenProxyService) {
            await service.commitCreatedFromParent(oldParentId, parent);
        }
    }


    async commitCreatedEntity(entity: T) {
        this.log("commitCreatedEntity()" + entity.id + ")");
        this.localService.remove(entity);
        let oldId = entity.id;
        entity.id = null;
        const newEntityResponse = await this.serverService.create(entity).toPromise();
        this.log("commitCreatedEntity(): created[" + newEntityResponse.body.id + "]");
        this.localService.add([newEntityResponse.body]);
        await this.commitCreatedChildren(oldId, newEntityResponse.body);
    }

    async commitCreatedChrildrenEntities() {
        this.log("commitCreatedChrildrenEntities()");
        for (let service of this.childrenProxyService) {
            await service.commitCreatedEntities();
        }
    }

    async commitCreatedEntities() {
        this.log("commitCreatedEntities()");
        let entities = this.localService.findWithStatus(SyncStatus.CREATE);
        for (let entity of entities) {
            await this.commitCreatedEntity(entity);
        }
        await this.commitCreatedChrildrenEntities();

    }


    async commitDeletedFromParent(parent: P[]) {
        this.log("commitDeletedFromParent(" + parent + ")");
        const entitiesResponse = await this.localService.findWhereParent(parent).toPromise();
        // const deletedEntities = entitiesResponse.body.filter(e => e.syncStatus == SyncStatus.DELETE);

        await this.commitDeletedEntity(entitiesResponse.body);

    }

    async commitDeletedChildren(parent: T[]) {
        this.log("commitDeletedChildren(" + parent + ")");
        for (let service of this.childrenProxyService) {
            await service.commitDeletedFromParent(parent);
        }
    }

    async commitDeletedEntity(entities: T[]) {
        if (entities && entities.length != 0) {
            this.log("commitDeletedEntity(" + entities + ")");
            await this.commitDeletedChildren(entities);
            for (let entity of entities) {
                if (entity.id > 0) {
                    this.log("commitDeletedEntity(): delete[" + entity + "]");
                    await this.serverService.delete(entity.id).toPromise();
                }
                this.localService.remove(entity);
            }
        }

    }


    async commitDeletedChildrenEntities() {
        this.log("commitDeletedChildrenEntities()");
        for (let service of this.childrenProxyService) {
            await service.commitDeletedEntities();
        }
    }

    async commitDeletedEntities() {
        this.log("commitDeletedEntities()");
        await this.commitDeletedChildrenEntities();
        let entities = this.localService.findWithStatus(SyncStatus.DELETE);
        await this.commitDeletedEntity(entities);
    }

    async commitChanges() {
        this.log("commitChanges()");
        await this.commitUpdatedEntities();
        await this.commitDeletedEntities();
        await this.commitCreatedEntities();
    }

    async loadDataFromServer(isRoot: boolean, parents?: P[], root?: R) {
        const entityResponse = await this.serverService.findWhereParent(parents, root).toPromise();
        const entities = entityResponse.body;
        if (isRoot && entities && entities.length != 1) {
            throw new Error("A root object must retrieve only one instance and it retrieve : " + entities.length);
        }
        this.localService.add(entities);
        for (let service of this.childrenProxyService) {
            if (isRoot) {
                await service.loadDataFromServer(false, entities, entities[0]);
            } else {
                await service.loadDataFromServer(false, entities, root);
            }
        }
    }

    clear() {
        this.localService.clear();
        for (let service of this.childrenProxyService) {
            service.clear();
        }
    }


}
