import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet } from 'src/app/pages/entities/wallet';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentConfigService } from '../mha-spent.-config.service/mha-spent-config.service';
import { MhaSpentService } from '../mha-spent.service/mha-spent.service';
import { MhaWalletLocalService } from './mha-wallet-local.service';
import { MhaWalletServerService } from './mha-wallet-server.service';

@Injectable({ providedIn: 'root' })
export class MhaWalletService extends MhaProxyClassService<Wallet, Workspace, Profil> {

    constructor(protected walletLocalService: MhaWalletLocalService,
        protected walletServerService: MhaWalletServerService,
        protected spentService: MhaSpentService,
        protected spentConfigService: MhaSpentConfigService) {
        super(walletLocalService, walletServerService, [spentService, spentConfigService]);
    }

}