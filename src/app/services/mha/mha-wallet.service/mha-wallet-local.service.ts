import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaWalletInterfaceService } from './mha-wallet-interface.service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet } from 'src/app/pages/entities/wallet';


@Injectable({ providedIn: 'root' })
export class MhaWalletLocalService extends MhaEntityLocalImplService<Wallet, Workspace, Profil> implements MhaWalletInterfaceService  {
    

    @LocalStorage('dbWallet')
    entities: Wallet[];

    @LocalStorage('dbWalletId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.workspaceId == oldParentId) {
                t.workspaceId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: Workspace[]): Observable<HttpResponse<Wallet[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(wallet => parents.some(p => {return p.id == wallet.workspaceId} )) }
        ));
    }
    sort() {
        // does nothing
    }
    
}