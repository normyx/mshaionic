import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet, WalletService } from 'src/app/pages/entities/wallet';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';

@Injectable({ providedIn: 'root' })
export class MhaWalletServerService extends WalletService implements MhaEntityServerInterfaceService<Wallet, Workspace, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Wallet, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], profil: Profil): Observable<HttpResponse<Wallet[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'workspaceId.in', profil, 'ownerId.equals');
    }

    

}
