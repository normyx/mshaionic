import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentLocalService } from './mha-spent-local.service';
import { MhaSpentServerService } from './mha-spent-server.service';
import { Spent } from 'src/app/pages/entities/spent';
import { MhaSpentSharingService } from '../mha-spent-sharing.service/mha-spent-sharing.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentService extends MhaProxyClassService<Spent, Wallet, Profil> {

    constructor(protected spentLocalService: MhaSpentLocalService,
        protected spentServerService: MhaSpentServerService,
        protected spentSharingService: MhaSpentSharingService) {
        super(spentLocalService, spentServerService, [spentSharingService]);
    }

}