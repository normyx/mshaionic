import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaSpentInterfaceService extends MhaEntityInterfaceService<Spent, Wallet, Profil> {
    
}
