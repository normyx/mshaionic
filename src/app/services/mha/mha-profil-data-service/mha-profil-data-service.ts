import { Injectable } from '@angular/core';
import { ProfilData } from 'src/app/pages/entities/profil-data';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaProfilDataLocalService } from './mha-profil-data-local-service';
import { MhaProfilDataServerService } from './mha-profil-data-server-service';
import { Profil } from 'src/app/pages/entities/profil';

@Injectable({ providedIn: 'root' })
export class MhaProfilDataService extends MhaProxyClassService<ProfilData, Workspace, Profil> {

    constructor(protected profilDataLocalService: MhaProfilDataLocalService,
        protected profilDataServerService: MhaProfilDataServerService) {
            super(profilDataLocalService, profilDataServerService, [])
    }

    

    async commitChanges() {
        throw new Error("commitChanges not implemented")
    }

}