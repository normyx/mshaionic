import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ProfilData, ProfilDataService } from 'src/app/pages/entities/profil-data';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaProfilDataInterfaceService } from './mha-profil-data-interface-service';
import { ApiService } from '../../api/api.service';
import { parseAsync } from '@babel/core';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';

@Injectable({ providedIn: 'root' })
export class MhaProfilDataServerService extends ProfilDataService implements MhaEntityServerInterfaceService<ProfilData, Workspace, Profil>, MhaProfilDataInterfaceService {
    constructor(protected http: HttpClient) {
        super(http);
    }
    findWhereParent(parents: Workspace[]): Observable<HttpResponse<ProfilData[]>> {
        if (!parents || parents.length == 0) return of(new HttpResponse({body:[]}));
        let parentIds : number[] = [];
        for (let workspace of parents) {
            parentIds.push(workspace.id);
        }
        return this.http.get<ProfilData[]>(ApiService.API_URL + `/mysha-profil-data-where-workspace/${parentIds}`, { observe: 'response' });
    }

    
}

