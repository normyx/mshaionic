import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { ProfilData } from 'src/app/pages/entities/profil-data';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaProfilDataInterfaceService } from './mha-profil-data-interface-service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';


@Injectable({ providedIn: 'root' })
export class MhaProfilDataLocalService extends MhaEntityLocalImplService<ProfilData, Workspace, Profil> implements MhaProfilDataInterfaceService   {
    
    

    @LocalStorage('dbProfilData')
    entities: ProfilData[];

    @LocalStorage('dbProfilDataId')
    newId: number;

    constructor() {
        super();
    }
    
    changeParentId(oldParentId: number, newParentId: number) {
        throw new Error("Method not implemented.");
    }
    findWhereParent(parents: Workspace[]): Observable<HttpResponse<ProfilData[]>> {
        throw new Error("Method not implemented.");
    }    

    sort() {
        // does nothing
    }
}