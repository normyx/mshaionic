import { Injectable } from '@angular/core';
import { MhaActiveProfilService } from './mha-active-profil.service/mha-active-profil.service';

@Injectable({ providedIn: 'root' })
export class MhaAsyncService {

    constructor(
        protected profilService: MhaActiveProfilService) {
    }

    public async refresh() {
        await this.profilService.commitChanges();
        await this.clear();
        await this.profilService.loadDataFromServer(true);
    }

    public async clear() {
        await this.profilService.clear();
    }

}