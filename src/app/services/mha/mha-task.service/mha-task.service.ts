import { Injectable } from '@angular/core';
import { Task } from '../../../pages/entities/task';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTaskLocalService } from './mha-task-local.service';
import { MhaTaskServerService } from './mha-task-server.service';
import { Profil } from 'src/app/pages/entities/profil';

@Injectable({ providedIn: 'root' })
export class MhaTaskService extends MhaProxyClassService<Task, TaskProject, Profil >  {

    constructor(protected taskLocalService: MhaTaskLocalService,
        protected taskServerService: MhaTaskServerService) {
            super(taskLocalService, taskServerService, [])
    }
}