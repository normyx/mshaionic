import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { Task, TaskService } from '../../../pages/entities/task';
import { TaskProject } from 'src/app/pages/entities/task-project';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';

@Injectable({ providedIn: 'root' })
export class MhaTaskServerService extends TaskService implements MhaEntityServerInterfaceService<Task, TaskProject, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Task, TaskProject, Profil>) {
        super(http);
    }

    findWhereParent(parents: TaskProject[]): Observable<HttpResponse<Task[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'taskProjectId.in');
    }

}
