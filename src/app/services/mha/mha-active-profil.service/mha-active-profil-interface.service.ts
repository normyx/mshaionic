import { Profil } from 'src/app/pages/entities/profil';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaActiveProfilInterfaceService extends MhaEntityInterfaceService<Profil, Profil, Profil> {
    
}
