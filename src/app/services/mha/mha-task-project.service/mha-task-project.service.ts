import { Injectable } from '@angular/core';
import { Workspace } from 'src/app/pages/entities/workspace';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTaskService } from '../mha-task.service/mha-task.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTaskProjectLocalService } from './mha-task-project-local.service';
import { MhaTaskProjectServerService } from './mha-task-project-server.service';
import { Profil } from 'src/app/pages/entities/profil';

@Injectable({ providedIn: 'root' })
export class MhaTaskProjectService extends MhaProxyClassService<TaskProject, Workspace, Profil> {

    constructor(protected taskProjectLocalService: MhaTaskProjectLocalService,
        protected taskProjectServerService: MhaTaskProjectServerService,
        protected workspaceService: MhaWorkspaceLocalService,
        protected taskService: MhaTaskService) {
        super(taskProjectLocalService, taskProjectServerService, [taskService])
    }

}