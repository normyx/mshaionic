import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-interface.service';
import { Profil } from 'src/app/pages/entities/profil';

export interface MhaWorkspaceInterfaceService extends MhaEntityInterfaceService<Workspace, Profil, Profil> {
    
}
