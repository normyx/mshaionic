import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Workspace, WorkspaceService } from 'src/app/pages/entities/workspace';
import { ApiService } from 'src/app/services/api/api.service';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';

@Injectable({ providedIn: 'root' })
export class MhaWorkspaceServerService extends WorkspaceService implements MhaEntityServerInterfaceService<Workspace, Profil, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Workspace, Profil, Profil>) {
        super(http);
    }
    
    findWhereParent(parents: Profil[]): Observable<HttpResponse<Workspace[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'ownerId.in');
    }


}
