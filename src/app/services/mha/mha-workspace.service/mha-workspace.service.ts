import { Injectable } from '@angular/core';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTaskProjectService } from '../mha-task-project.service/mha-task-project.service';
import { MhaWalletService } from '../mha-wallet.service/mha-wallet.service';
import { MhaWorkspaceLocalService } from './mha-workspace-local.service';
import { MhaWorkspaceServerService } from './mha-workspace-server.service';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaProfilDataService } from '../mha-profil-data-service/mha-profil-data-service';

@Injectable({ providedIn: 'root' })
export class MhaWorkspaceService extends MhaProxyClassService<Workspace, Profil, Profil> {

    constructor(protected workspaceLocalService: MhaWorkspaceLocalService,
        protected workspaceServerService: MhaWorkspaceServerService,
        protected taskProjectService: MhaTaskProjectService,
        protected walletService: MhaWalletService,
        protected profilDataService: MhaProfilDataService) {
        super(workspaceLocalService, workspaceServerService, [taskProjectService, walletService, profilDataService])
    }


    public setStoredActiveWorkspace(workspace: Workspace) {
        this.workspaceLocalService.setStoredActiveWorkspace(workspace);
    }

    public getStoredActiveWorkspace(): Workspace {
        return this.workspaceLocalService.getStoredActiveWorkspace();
    }
}