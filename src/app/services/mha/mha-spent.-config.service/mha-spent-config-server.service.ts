import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { SpentConfigService, SpentConfig } from 'src/app/pages/entities/spent-config';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigServerService extends SpentConfigService implements MhaEntityServerInterfaceService<SpentConfig, Wallet, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<SpentConfig, Wallet, Profil>) {
        super(http);
    }

    findWhereParent(parents: Wallet[], profil: Profil): Observable<HttpResponse<SpentConfig[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents, 'walletId.in');
    }

    

}
