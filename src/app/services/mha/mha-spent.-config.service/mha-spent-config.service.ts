import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentConfigLocalService } from './mha-spent-config-local.service';
import { MhaSpentConfigServerService } from './mha-spent-config-server.service';
import { MhaSpentSharingConfigService } from '../mha-spent-sharing-config.service/mha-spent-sharing-config.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigService extends MhaProxyClassService<SpentConfig, Wallet, Profil> {

    constructor(protected spentConfigLocalService: MhaSpentConfigLocalService,
        protected spentConfigServerService: MhaSpentConfigServerService,
        protected spentSharingConfigService: MhaSpentSharingConfigService) {
        super(spentConfigLocalService, spentConfigServerService, [spentSharingConfigService]);
    }

}