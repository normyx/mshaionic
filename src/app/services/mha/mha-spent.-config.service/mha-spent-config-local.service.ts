import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaSpentConfigInterfaceService } from './mha-spent-config-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaSpentConfigLocalService extends MhaEntityLocalImplService<SpentConfig, Wallet, Profil> implements MhaSpentConfigInterfaceService  {
    

    @LocalStorage('dbSpentConfig')
    entities: SpentConfig[];

    @LocalStorage('dbSpentConfigId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.walletId == oldParentId) {
                t.walletId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: Wallet[]): Observable<HttpResponse<SpentConfig[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(e => parents.some(p => {return p.id == e.walletId} )) }
        ));
    }
    sort() {
        // does nothing
    }
    
}