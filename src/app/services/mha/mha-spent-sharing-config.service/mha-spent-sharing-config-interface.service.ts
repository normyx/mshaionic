import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { SpentSharingConfig } from 'src/app/pages/entities/spent-sharing-config';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaSpentSharingConfigInterfaceService extends MhaEntityInterfaceService<SpentSharingConfig, SpentConfig, Profil> {
    
}
