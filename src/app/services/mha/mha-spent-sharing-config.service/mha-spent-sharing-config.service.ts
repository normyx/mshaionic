import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { SpentSharingConfig } from 'src/app/pages/entities/spent-sharing-config';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentSharingConfigLocalService } from './mha-spent-sharing-config-local.service';
import { MhaSpentSharingConfigServerService } from './mha-spent-sharing-config-server.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentSharingConfigService extends MhaProxyClassService<SpentSharingConfig, SpentConfig, Profil> {

    constructor(protected spentSharingConfigLocalService: MhaSpentSharingConfigLocalService,
        protected spentSharingConfigServerService: MhaSpentSharingConfigServerService) {
        super(spentSharingConfigLocalService, spentSharingConfigServerService, []);
    }

}