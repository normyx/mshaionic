import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { SpentSharing, SpentSharingService } from 'src/app/pages/entities/spent-sharing';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentSharingServerService extends SpentSharingService implements MhaEntityServerInterfaceService<SpentSharing, Spent, Profil> {

    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<SpentSharing, Spent, Profil>) {
        super(http);
    }

    findWhereParent(parents: Spent[], profil: Profil): Observable<HttpResponse<SpentSharing[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'spentId.in');
    }

}
