import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Profil, ProfilService } from 'src/app/pages/entities/profil';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaWorkspaceService } from '../mha-workspace.service/mha-workspace.service';
import { MhaProfilInterfaceService } from './mha-profil-interface.service';

@Injectable({ providedIn: 'root' })
export class MhaProfilServerService extends ProfilService implements MhaEntityServerInterfaceService<Profil, Profil, Profil>, MhaProfilInterfaceService {


    constructor(protected http: HttpClient, protected workspaceService: MhaWorkspaceService) {
        super(http);
    }

    findWhereParent(parents: Profil[], root?: Profil): Observable<HttpResponse<Profil[]>> {
        return this.workspaceService.findWhereParent(parents, root).pipe(
            mergeMap((res: HttpResponse<Workspace[]>) => {
                let wIds: number[] = [];
                for (let w of res.body) {
                    wIds.push(w.id);
                }
                return this.query({ "workspaceId.in": wIds });

            }));
    }

   

}
