import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaProfilInterfaceService } from './mha-profil-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaProfilLocalService extends MhaEntityLocalImplService<Profil, Profil, Profil> implements MhaProfilInterfaceService  {
    
    @LocalStorage('dbProfil')
    entities: Profil[];

    @LocalStorage('dbProfilId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Profil[], root?: Profil): Observable<HttpResponse<Profil[]>> {
        return of(new HttpResponse(
            { body: this.entities }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        throw new Error("No Parent for Profil. Root of the tree !");
    }

    sort() {
        // does nothing
    }

    
}