export function getPriorityColor(priotity: number): string {
    switch (priotity) {
        case 1: {
            return 'danger';
        }
        case 2: {
            return 'primary';
        }
        case 3: {
            return 'secondary';
        }
        case 4: {
            return 'light';
        }

    }
}