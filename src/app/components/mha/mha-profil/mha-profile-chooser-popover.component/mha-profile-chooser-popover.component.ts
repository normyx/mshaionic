import { Component, OnInit } from '@angular/core';
import { Profil } from '../../../../pages/entities/profil';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-mha-profile-chooser-popover',
  templateUrl: './mha-profile-chooser-popover.component.html',
  styleUrls: ['./mha-profile-chooser-popover.component.scss'],
})
export class MhaProfileChooserPopoverComponent implements OnInit {

  profils: Profil[];
  availableProfils: Profil[];
  warningIfConnectedUserRemoved: boolean;
  canBeWithNoActor: boolean;



  constructor(private popController: PopoverController, private navParams: NavParams) { }

  ngOnInit() {
    this.profils = this.navParams.data.profils;
    this.availableProfils = this.navParams.data.availableProfils;
    this.warningIfConnectedUserRemoved = this.navParams.data.warningIfConnectedUserRemoved;
    this.canBeWithNoActor = this.navParams.data.canBeWithNoActor;
    if (!this.canBeWithNoActor && this.canBeWithNoActor != false) {
      this.canBeWithNoActor = true;
    }
    if (!this.availableProfils) {
      this.availableProfils = new Array<Profil>();
    }
    if (!this.warningIfConnectedUserRemoved) {
      this.warningIfConnectedUserRemoved = false;
    }
    if (!this.profils) {
      this.profils = new Array<Profil>();
    }
  }

  canRemove(): boolean {
    return this.canBeWithNoActor || this.profils.length != 1;
  }

  removeActor(actor: Profil) {
    this.profils.splice(this.profils.findIndex(x => x.id == actor.id), 1);
    this.availableProfils.push(actor);
  }

  addActor(actor: Profil) {
    this.profils.push(actor);
    this.availableProfils.splice(this.availableProfils.findIndex(x => x.id == actor.id), 1);
  }

  async close() {
    await this.popController.dismiss(this.profils);
  }
}
