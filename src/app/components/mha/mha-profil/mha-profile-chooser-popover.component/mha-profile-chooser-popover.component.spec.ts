import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhaProfileChooserPopoverComponent } from './mha-profile-chooser-popover.component';

describe('MhaProfileChooserPopoverComponent', () => {
  let component: MhaProfileChooserPopoverComponent;
  let fixture: ComponentFixture<MhaProfileChooserPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhaProfileChooserPopoverComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhaProfileChooserPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
