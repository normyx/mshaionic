import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/pages/entities/task';
import { NavController } from '@ionic/angular';
import { MhaTaskService } from 'src/app/services/mha/mha-task.service/mha-task.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { getPriorityColor } from 'src/app/shared/util/helpers';

@Component({
  selector: 'app-mha-task-list-item',
  templateUrl: './mha-task-list-item.component.html',
  styleUrls: ['./mha-task-list-item.component.scss'],
})
export class MhaTaskListItemComponent implements OnInit {

  @Input()
  task: Task;

  constructor(
    private navController: NavController,
    private taskService: MhaTaskService) {

  }

  ngOnInit() { }  

  openTask() {
    //slidingItem.close();
    this.navController.navigateForward('/menu/task/' + this.task.id + '/edit');
  }
  getPriorityColor(): string {
    return getPriorityColor(this.task.priority);
  }

  updateTask() {
    this.taskService.update(this.task).subscribe((res: HttpResponse<Task>) => { }, (res: HttpErrorResponse) => { });
  }


}
