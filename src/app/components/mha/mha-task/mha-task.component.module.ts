
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaTaskListItemComponent } from './mha-task-list-item.component/mha-task-list-item.component';
import { MhaProfilDataComponentModule } from 'src/app/components/mha/mha-profil-data/mha-profil-data.component.module';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MhaProfilDataComponentModule,
    IonicModule],
  declarations: [MhaTaskListItemComponent],
  entryComponents: [MhaTaskListItemComponent],
  exports: [MhaTaskListItemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaTaskComponentModule {
  constructor() {
  }
}
