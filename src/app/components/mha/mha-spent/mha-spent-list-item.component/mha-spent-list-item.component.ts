import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { NavController, AlertController, Events } from '@ionic/angular';
import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { MhaProfilService } from 'src/app/services/mha/mha-profil.service/mha-profil.service';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';

@Component({
  selector: 'app-mha-spent-list-item',
  templateUrl: './mha-spent-list-item.component.html',
  styleUrls: ['./mha-spent-list-item.component.scss'],
})
export class MhaSpentListItemComponent implements OnInit, OnChanges {

  @Input()
  spent: Spent;

  spender: Profil;

  constructor(
    private navController: NavController,
    private spentService: MhaSpentService,
    private profilService: MhaProfilService,
    private alertController: AlertController,
    public events: Events
  ) {

  }

  ngOnInit() {
    this.loadAll();
  }

  ngOnChanges(): void {
    this.loadAll();
  }

  loadAll() {
    if (this.spent) {
      this.profilService.find(this.spent.spenderId).subscribe(
        (response => {
          this.spender = response.body;
        })
      )
    }
  }


  openSpent() {
    this.navController.navigateForward('/menu/spent/' + this.spent.id + '/edit');
  }

  async deleteSpentModal(slidingItem) {
    slidingItem.close();
    const alert = await this.alertController.create({
      header: 'Confirmer vous la suppression de ' + this.spent.label + '?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Supprimer',
          handler: () => {
            
            this.spentService.delete(this.spent.id).subscribe(() => {
              this.events.publish('updateWallet');

            }); 

          }
        }
      ]
    });
    await alert.present();
  }

  /*updateSpent() {
    this.spentService.update(this.spent).subscribe((res: HttpResponse<Spent>) => { }, (res: HttpErrorResponse) => { });
  }*/


}
