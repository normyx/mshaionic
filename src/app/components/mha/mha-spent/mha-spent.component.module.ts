
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaProfilDataComponentModule } from 'src/app/components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaSpentListItemComponent } from './mha-spent-list-item.component/mha-spent-list-item.component';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MhaProfilDataComponentModule,
    IonicModule],
  declarations: [MhaSpentListItemComponent],
  entryComponents: [MhaSpentListItemComponent],
  exports: [MhaSpentListItemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaSpentComponentModule {
  constructor() {
  }
}
