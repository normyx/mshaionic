
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaCompProfilDataAvatarComponent } from './mha-profil-data-avatar.component/mha-profil-data-avatar.component';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule],
  declarations: [MhaCompProfilDataAvatarComponent],
  entryComponents: [MhaCompProfilDataAvatarComponent],
  exports: [MhaCompProfilDataAvatarComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaProfilDataComponentModule {
  constructor() {
  }
}
